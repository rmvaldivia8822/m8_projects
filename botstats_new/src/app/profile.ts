export class Profile {
  id: number;
  leads_id: number;
  platform: string;
  scoped_id: string;
  research: string;
  extended_profile: string;
}