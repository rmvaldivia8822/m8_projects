import { Component, ViewContainerRef, ViewChild } from '@angular/core';

import { GlobalState } from './global.state';
import { BaImageLoaderService, BaThemePreloader, BaThemeSpinner } from './theme/services';
import { BaThemeConfig } from './theme/theme.config';
import { layoutPaths } from './theme/theme.constants';

import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';

import { ModalDirective } from 'ng2-bootstrap';

import { Router } from '@angular/router';


import 'style-loader!./app.scss';
import 'style-loader!./theme/initial.scss';

import { environment } from '../environments/environment';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./modals.scss'],
})
export class App {

  isMenuCollapsed: boolean = false;

  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  @ViewChild('smModal') childModal: ModalDirective;

  constructor(private _state: GlobalState,
              private _imageLoader: BaImageLoaderService,
              private _spinner: BaThemeSpinner,
              private viewContainerRef: ViewContainerRef,
              private themeConfig: BaThemeConfig,
              private idle: Idle,
              private keepalive: Keepalive,
              private router: Router) {

    themeConfig.config();

    this._loadImages();

    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });

    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(environment.idle_timeout);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(environment.idle_timeout_warning);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      this.logout();
    });
    idle.onIdleStart.subscribe(
      () => {
          this.idleState = 'You\'ve gone idle!'
          this.showChildModal();
      }
    );
    idle.onTimeoutWarning.subscribe(
      (countdown) => {
        this.idleState = 'You will time out in ' + countdown + ' seconds!'
      }
    );

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => this.lastPing = new Date());

    this.reset();
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  showChildModal(): void {
    let url : any = this.router.url;
    if ( url != "/login" && url.indexOf('/tools/service-center/user-search') != 0 ) {
        this.childModal.show();
    }
  }

  hideChildModal(): void {
    this.reset();
    this.childModal.hide();
  }

  public logout (): void {
    this.hideChildModal();
    this.router.navigateByUrl('/login');
  }

  public ngAfterViewInit(): void {
    // hide spinner once all loaders are completed
    BaThemePreloader.load().then((values) => {
      this._spinner.hide();
    });
  }

  private _loadImages(): void {
    // register some loaders
    BaThemePreloader.registerLoader(this._imageLoader.load(layoutPaths.images.root + 'sky-bg.jpg'));
  }

}
