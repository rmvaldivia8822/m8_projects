import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { Distributors } from '../distributors';

@Injectable()
export class DistributorsService {

  private api_url = `${environment.wepa_url}api/distributors`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getDistributors(page?: number): Promise<Distributors[]> {
    let api_url = this.api_url;
    if (page) {
      api_url = `${api_url}?page=${page}`;
    }
      return this.http
          .get(api_url, {headers: this.headers})
          .toPromise()
          .then(response => response.json().leads.data as Distributors[])
          .catch(this.handleError);      
  }


  getDistributor(distributor_id: number): Promise<Distributors> {
    return this.http
            .get(`${this.api_url}/${distributor_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().distributor)
            .catch(this.handleError);        
    
  } 

  deleteDistributor(distributor_id: number): Promise<Distributors> {
    return this.http
            .delete(`${this.api_url}/${distributor_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().error)
            .catch(this.handleError);        
    
  } 

  updateDistributor(distributor: any): Promise<Distributors> {
    let distributor_id = distributor.id;    
    let body = JSON.stringify(distributor);

    return this.http
            .put(`${this.api_url}/${distributor_id}`, body, {headers: this.headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);        
    
  } 

  createDistributor(distributor: any): Promise<Distributors> {
    let body = JSON.stringify(distributor);
    return this.http
            .post(this.api_url, body, {headers: this.headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);        
    
  } 

  getDistributorsToPdf(query:string): Promise<Distributors[]> {

    let body = JSON.stringify(this.getUrlVars(query));

    return this.http
          .post(`${this.api_url}/export-pdf`, body ,{headers: this.headers})
          .toPromise()
          .then(response => response.json().distributors as Distributors[])
          .catch(this.handleError);      
  }

  getUrlVars(url:string): any {
    let vars = {};
    url.replace(/([^=&]+)=([^&]*)/gi, function dd(m, key, value): any {
         key = decodeURIComponent(key);
         value = decodeURIComponent(value);
         vars[key] = value;
    });
    return vars;
}

}
