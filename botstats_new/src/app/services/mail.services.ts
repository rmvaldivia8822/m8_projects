import { Injectable } from '@angular/core';
import { Http , URLSearchParams , Response, Headers  } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { environment } from '../../environments/environment';


@Injectable()
export class MailService {
  private sendmail_url = `${environment.wepa_url}api/sendmail`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(public http: Http) {}

  sendMail(to, body) : Observable<any> {

      let params: URLSearchParams = new URLSearchParams();
     params.append('email_to', to );
     params.append('email_from', environment.email_from );
     params.append('body', body );

    return this.http.post(this.sendmail_url , params, {
                   headers: this.headers})
                   .map((response: Response) => response.json() )
                   .do(data => console.log('All: ' +  JSON.stringify(data)))
                   .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }
}