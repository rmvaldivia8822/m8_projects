import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { Profile } from '../profile';

@Injectable()
export class ProfilesService {

  private api_url = `${environment.wepa_url}api/profiles`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getProfiles(page?: number): Promise<Profile[]> {
    let api_url = this.api_url;
    if (page) {
      api_url = `${api_url}?page=${page}`;
    }
      return this.http
          .get(api_url, {headers: this.headers})
          .toPromise()
          .then(response => response.json().profiles.data as Profile[])
          .catch(this.handleError);      
  }


  getProfile(profile_id: number): Promise<Profile> {
    return this.http
            .get(`${this.api_url}/${profile_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().profile)
            .catch(this.handleError);        
    
  } 

  getProfilesToPdf(query:string): Promise<Profile[]> {
    
    let body = JSON.stringify(this.getUrlVars(query));

    return this.http
          .post(`${this.api_url}/export-pdf`, body ,{headers: this.headers})
          .toPromise()
          .then(response => response.json().profiles as Profile[])
          .catch(this.handleError);      
  }

  getUrlVars(url:string): any {
    let vars = {};
    url.replace(/([^=&]+)=([^&]*)/gi, function dd(m, key, value): any{
         key = decodeURIComponent(key);
         value = decodeURIComponent(value);
         vars[key] = value;
    });
    return vars;
  }


}
