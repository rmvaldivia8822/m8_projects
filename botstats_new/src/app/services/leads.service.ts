import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { Lead } from '../lead';
import { Tool } from '../tool';
import { Action } from '../action';

@Injectable()
export class LeadsService {

  private api_url = `${environment.wepa_url}api/leads`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getLeads(page?: number): Promise<Lead[]> {
    let api_url = this.api_url;
    if (page) {
      api_url = `${api_url}?page=${page}`;
    }
      return this.http
          .get(api_url, {headers: this.headers})
          .toPromise()
          .then(response => response.json().leads.data as Lead[])
          .catch(this.handleError);      
  }


  getLead(lead_id: number): Promise<Lead> {
    return this.http
            .get(`${this.api_url}/${lead_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().lead)
            .catch(this.handleError);        
    
  } 

  getLeadTools(lead_id: number): Promise<Tool[]> {    
    return this.http
    .get(`${this.api_url}/${lead_id}/tools`, {headers: this.headers})
    .toPromise()
    .then(response => response.json().tools as Tool[])
    .catch(this.handleError);
    
  }

  getLeadActions(lead_id: number): Promise<Action[]> {    
    return this.http
    .get(`${this.api_url}/${lead_id}/actions`, {headers: this.headers})
    .toPromise()
    .then(response => response.json().actions as Action[])
    .catch(this.handleError);
    
  }

  getLeadProfileComfirmed(lead_id: number): Promise<number> {    
    return this.http
    .get(`${this.api_url}/${lead_id}/confirmed`, {headers: this.headers})
    .toPromise()
    .then(response => response.json().confirmed_info)
    .catch(this.handleError);
    
  }

  getLeadsToPdf(query:string): Promise<Lead[]> {
    
    let body = JSON.stringify(this.getUrlVars(query));

    return this.http
          .post(`${this.api_url}/export-pdf`, body ,{headers: this.headers})
          .toPromise()
          .then(response => response.json().leads as Lead[])
          .catch(this.handleError);      
  }

  search(search: string): Promise<Lead[]> {
    let param = {
      search: search
    };
    let body = JSON.stringify(param);
    return this.http
          .post(`${this.api_url}/search`, body ,{headers: this.headers})
          .toPromise()
          .then(response => response.json().leads as Lead[])
          .catch(this.handleError);      
  }

  getUrlVars(url:string): any {
    let vars = {};
    url.replace(/([^=&]+)=([^&]*)/gi, function dd(m, key, value): any{
         key = decodeURIComponent(key);
         value = decodeURIComponent(value);
         vars[key] = value;
    });
    return vars;
  }

  updateLead(lead: any): Promise<Lead> {
    let lead_id = lead.id;    
    let body = JSON.stringify(lead);

    return this.http
            .put(`${this.api_url}/${lead_id}`, body, {headers: this.headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);        
    
  } 


}
