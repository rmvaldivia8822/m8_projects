import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { Action } from '../action';

@Injectable()
export class ActionsServices {

  private api_url = `${environment.wepa_url}api/actions`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getTotalsActions(): Promise<any> {
    return this.http
        .get(`${this.api_url}/totals`, {headers: this.headers})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);          
  }

}
