import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { ServiceCenters } from '../servicecenter';

@Injectable()
export class ServiceCentersService {

  private api_url = `${environment.wepa_url}api/servicecenters`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getServiceCenters(page?: number): Promise<ServiceCenters[]> {
    let api_url = this.api_url;
    if (page) {
      api_url = `${api_url}?page=${page}`;
    }
      return this.http
          .get(api_url, {headers: this.headers})
          .toPromise()
          .then(response => response.json().servicecenters.data as ServiceCenters[])
          .catch(this.handleError);      
  }


  getServiceCenter(scenter_id: number): Promise<ServiceCenters> {
    return this.http
            .get(`${this.api_url}/${scenter_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().servicecenter)
            .catch(this.handleError);        
    
  } 

  deleteServiceCenter(scenter_id: number): Promise<ServiceCenters> {
    return this.http
            .delete(`${this.api_url}/${scenter_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().error)
            .catch(this.handleError);        
    
  } 

  updateServiceCenter(scenter: any): Promise<ServiceCenters> {
    let scenter_id = scenter.id;    
    let body = JSON.stringify(scenter);

    return this.http
            .put(`${this.api_url}/${scenter_id}`, body, {headers: this.headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);        
    
  } 

  createServiceCenter(scenter: any): Promise<ServiceCenters> {
    let body = JSON.stringify(scenter);
    return this.http
            .post(this.api_url, body, {headers: this.headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);        
    
  } 

  getServiceCenterToPdf(query:string): Promise<ServiceCenters[]> {
    
    let body = JSON.stringify(this.getUrlVars(query));
    
    return this.http
          .post(`${this.api_url}/export-pdf`, body ,{headers: this.headers})
          .toPromise()
          .then(response => response.json().servicecenters as ServiceCenters[])
          .catch(this.handleError);      
  }

  getUrlVars(url:string): any {
    let vars = {};
    url.replace(/([^=&]+)=([^&]*)/gi, function dd(m, key, value): any {
         key = decodeURIComponent(key);
         value = decodeURIComponent(value);
         vars[key] = value;
    });    
    return vars;
  }

}
