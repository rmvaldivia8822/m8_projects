import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { Tool } from '../tool';

@Injectable()
export class ToolsService {

  private api_url = `${environment.wepa_url}api/tools`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getToolReceipts(tool_id: number): Promise<any> {
    return this.http
            .get(`${this.api_url}/${tool_id}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().receipt)
            .catch(this.handleError);        
    
  }

}
