import {
    Component,
    ViewChild,
    Input,
    Output,
    ElementRef,
    EventEmitter
} from '@angular/core';

import * as Chartist from 'chartist';
import 'style-loader!./baChartistChart.scss';

import { BotstatsService } from '../../../pages/dashboard/dashboard.sevices';

import * as moment from 'moment';

@Component({
  selector: 'ba-chartist-chart',
  templateUrl: './baChartistChart.html',
  providers: [],
})
export class BaChartistChart {

  @Input() stat:string;

  @Input() baChartistChartType:string;
  //@Input() baChartistChartData:Object;
  @Input() baChartistChartOptions:Object;
  @Input() baChartistChartResponsive:Object;
  @Input() baChartistChartClass:string;
  @Output() onChartReady = new EventEmitter<any>();

  @ViewChild('baChartistChart') public _selector: ElementRef;

  private chart;

  private baChartistChartData;

  constructor(private _db:BotstatsService) { 
    this.baChartistChartData = {
      labels: [],
      series: []
    };
  }

  ngAfterViewInit() {

    if ( this.stat == 'week' ) {
      this.baChartistChartData.labels = [];
      this.baChartistChartData.series = []; 
        this._db.getWeeklyStatsByDay().then(stats => {
        for (const value of stats.dates) {
            this.baChartistChartData.labels.push(moment(value).fromNow());
            
        }
        let leadStats = [];
        let toolStats = [];
        let referralStats = [];
        let actionStats = [];
        
        for (const prop in stats.data.leads.values) {
          leadStats.push(stats.data.leads.values[prop]);
          toolStats.push(stats.data.tools.values[prop]);
          referralStats.push(stats.data.referrals.values[prop]);
          actionStats.push(stats.data.actions.values[prop]);
        } 
        this.baChartistChartData.series.push(leadStats);
        this.baChartistChartData.series.push(toolStats);
        this.baChartistChartData.series.push(referralStats);
        this.baChartistChartData.series.push(actionStats);

        this.chart = new Chartist[this.baChartistChartType](this._selector.nativeElement, this.baChartistChartData, this.baChartistChartOptions, this.baChartistChartResponsive);
        this.onChartReady.emit(this.chart);

      });
    } else {

      this.baChartistChartData.labels = [];
      this.baChartistChartData.series = [];

      this._db.getTotalStatsByDay().then(stats => {
        if ( stats.dates.length < 6 ) {
          for (const value of stats.dates) {
            this.baChartistChartData.labels.push(moment(value).fromNow());
            
          }      
        } else {
          for ( let i=0; i<stats.dates.length; i++ ) {
              if (i%2 === 0)
                this.baChartistChartData.labels.push(moment(stats.dates[i]).fromNow());
              else {
                this.baChartistChartData.labels.push("");
              }  
          }  
        }

        let leadStats = [];
        let toolStats = [];
        let referralStats = [];
        let actionStats = [];
        
        for (const prop in stats.data.leads.values) {
          leadStats.push(stats.data.leads.values[prop]);
          toolStats.push(stats.data.tools.values[prop]);
          referralStats.push(stats.data.referrals.values[prop]);
          actionStats.push(stats.data.actions.values[prop]);
        } 
        this.baChartistChartData.series.push(leadStats);
        this.baChartistChartData.series.push(toolStats);
        this.baChartistChartData.series.push(referralStats);
        this.baChartistChartData.series.push(actionStats);        

        this.chart = new Chartist[this.baChartistChartType](this._selector.nativeElement, this.baChartistChartData, this.baChartistChartOptions, this.baChartistChartResponsive);
        this.onChartReady.emit(this.chart);

      });

    }    
  }

  ngOnChanges(changes) {
    if (this.chart) {
      if ( this.stat == 'week' ) {
        this.baChartistChartData.labels = [];
        this.baChartistChartData.series = []; 
          this._db.getWeeklyStatsByDay().then(stats => {
          for (const value of stats.dates) {
              this.baChartistChartData.labels.push(moment(value).fromNow());
              
          }
          let leadStats = [];
          let toolStats = [];
          let referralStats = [];
          let actionStats = [];
          
          for (const prop in stats.data.leads.values) {
            leadStats.push(stats.data.leads.values[prop]);
            toolStats.push(stats.data.tools.values[prop]);
            referralStats.push(stats.data.referrals.values[prop]);
            actionStats.push(stats.data.actions.values[prop]);
          } 
          this.baChartistChartData.series.push(leadStats);
          this.baChartistChartData.series.push(toolStats);
          this.baChartistChartData.series.push(referralStats);
          this.baChartistChartData.series.push(actionStats);
  
          (<any>this.chart).update(this.baChartistChartData, this.baChartistChartOptions);
  
        });
      } else {
  
        this.baChartistChartData.labels = [];
        this.baChartistChartData.series = [];
  
        this._db.getTotalStatsByDay().then(stats => {
        if ( stats.dates.length < 6 ) {
          for (const value of stats.dates) {
            this.baChartistChartData.labels.push(moment(value).fromNow());
            
          }      
        } else {
          for ( let i=0; i<stats.dates.length; i++ ) {
              if (i%2 === 0)
                this.baChartistChartData.labels.push(moment(stats.dates[i]).fromNow());
              else {
                this.baChartistChartData.labels.push("");
              }  
          }  
        }

          let leadStats = [];
          let toolStats = [];
          let referralStats = [];
          let actionStats = [];
          
          for (const prop in stats.data.leads.values) {
            leadStats.push(stats.data.leads.values[prop]);
            toolStats.push(stats.data.tools.values[prop]);
            referralStats.push(stats.data.referrals.values[prop]);
            actionStats.push(stats.data.actions.values[prop]);
          } 
          this.baChartistChartData.series.push(leadStats);
          this.baChartistChartData.series.push(toolStats);
          this.baChartistChartData.series.push(referralStats);
          this.baChartistChartData.series.push(actionStats);
  
          (<any>this.chart).update(this.baChartistChartData, this.baChartistChartOptions);
  
        });
  
      }      
    }
  }

  ngOnDestroy():void {
    if (this.chart) {
      this.chart.detach();
    }
  }
}
