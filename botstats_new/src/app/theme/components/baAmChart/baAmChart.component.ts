import {Component, ViewChild, Input, Output, ElementRef, EventEmitter} from '@angular/core';

import {BaThemePreloader} from '../../../theme/services';

import {BaThemeConfigProvider} from '../../../theme';

import { LeadsService } from '../../../services/leads.service';
import { environment } from '../../../../environments/environment';

import 'amcharts3';
import 'amcharts3/amcharts/plugins/responsive/responsive.js';
import 'amcharts3/amcharts/serial.js';

import 'ammap3';
import 'ammap3/ammap/maps/js/worldLow';


import {BaAmChartThemeService} from './baAmChartTheme.service';

import 'style-loader!./baAmChart.scss';

@Component({
  selector: 'ba-am-chart',
  templateUrl: './baAmChart.html',
  providers: [BaAmChartThemeService],
})
export class BaAmChart {

  @Input() baAmChartConfiguration:Object;
  @Input() baAmChartClass:string;
  @Output() onChartReady = new EventEmitter<any>();

  @ViewChild('baAmChart') public _selector:ElementRef;

  constructor (
    private _baAmChartThemeService:BaAmChartThemeService,
    private _baConfig:BaThemeConfigProvider,
    private ls: LeadsService) {
    this._loadChartsLib();
  }

  ngOnInit() {
    AmCharts.themes.blur = this._baAmChartThemeService.getTheme();
  }

  ngAfterViewInit() {
    if (this.baAmChartClass == 'dashboard-users-map') {
      var layoutColors = this._baConfig.get().colors;
      let countrys = [];
      let areas = [];
  
      var groupBy = function(xs, key) {
            return xs.reduce(function(rv, x) {
              (rv[x[key]] = rv[x[key]] || []).push(x);
              return rv;
            }, {});
          };
  
      this.ls.getLeadsToPdf("").then( data => {
            for ( let i=0; i<data.length; i++ ) {
                if ( data[i].country  ) {
                    countrys.push(data[i].country);
                }
            }
            var countedNames = countrys.reduce(function (allNames, name) { 
              if (name in allNames) {
                allNames[name]++;
              }
              else {
                allNames[name] = 1;
              }
              return allNames;
            }, {});
      
            for ( var key in countedNames ) {
              var value = countedNames[key];
              let area = {
                title: key, 
                id: environment.countrys[key], 
                color: layoutColors.primary, 
                customData: value, 
                groupId: '1'
              };
              areas.push(area);
            }

            this.baAmChartConfiguration['dataProvider']['areas'] = areas;
            
            let chart = AmCharts.makeChart(this._selector.nativeElement, this.baAmChartConfiguration);
            this.onChartReady.emit(chart);
        });
      
    }
    
  }

  private _loadChartsLib():void {
    BaThemePreloader.registerLoader(new Promise((resolve, reject) => {
      let amChartsReadyMsg = 'AmCharts ready';

      if (AmCharts.isReady) {
        resolve(amChartsReadyMsg);
      } else {
        AmCharts.ready(function () {
          resolve(amChartsReadyMsg);
        });
      }
    }));
  }
}
