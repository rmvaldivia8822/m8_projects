import {Component } from '@angular/core';
import { Router } from '@angular/router';

import { LocalDataSource } from '../../ng2-smart-table';

import 'style-loader!./usersearch.scss';

import { Lead } from '../../lead';
import { LeadsService } from '../../services/leads.service';


@Component({
  selector: 'usersearch',
  templateUrl: './usersearch.html',
})
export class Usersearch {

  search : string = "";
  leads : Lead[] = [];

  settings = {
    actions: null,
    pager: {
      perPage: 10
    },
    columns: {
      name: {
        title: 'Name',
        type: 'html',
        filter: false,
        valuePrepareFunction: (name) => {
          if (name.length <= 1) {
            return '<b class="text-rows">' + 'Unknown' + "</b>";
          }
          return '<b class="text-rows">' + name + "</b>" ;
        }
      },
      country: {
        title: 'From',
        type: 'html',
        filter: false,
        valuePrepareFunction: (country) => {
          if (!country)
            return 'Unknown';

          return country;
        }
      }
    }
  };

  source: LocalDataSource = new LocalDataSource();

  constructor( private service: LeadsService, private router: Router ){
  }

  searchButton():void {
    if ( this.search != "" ) {
      this.service.search(this.search).then( data => {
        this.source.load(data);
      });
    } else {
      this.source.empty();
      this.source.refresh();
    }
  }

  onUserRowSelect(event):void {

    this.router.navigate(['tools/service-center/user-search/'+event.data.id]);

  }
  
}
