import { Routes, RouterModule }  from '@angular/router';

import { Usersearch } from './usersearch.component';
import { ModuleWithProviders } from '@angular/core';

import { LeadDetailGuard } from '../tables/components/leadTables/lead-detail/lead-detail-guard.services';
import { LeadDetailsResolver } from '../tables/components/leadTables/lead-detail/lead-detail-resolver.services';

import { UsersearchDetailComponent } from './usersearch-detail/usersearch-detail.component';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: 'user-search',
    component: Usersearch
  },
  { path: 'user-search/:id', 
    component: UsersearchDetailComponent,
    canActivate: [LeadDetailGuard],
    resolve: {
      user: LeadDetailsResolver
    } 
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
