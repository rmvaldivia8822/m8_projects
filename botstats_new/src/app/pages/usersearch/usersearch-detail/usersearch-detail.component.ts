import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Lead } from '../../../lead';
import { Tool } from '../../../tool';
import { Action } from '../../../action';

import { LeadsService } from '../../../services/leads.service';
import { ToolsService } from '../../../services/tools.services';

import { ModalDirective } from 'ng2-bootstrap';

@Component({
  selector: 'usersearch-details',
  templateUrl: './usersearch-detail.html',
  styleUrls: ['./usersearch-detail.scss']
})
export class UsersearchDetailComponent implements OnInit {
  lead: Lead;
  tools: Tool[] = [];
  actions: Action[] = [];
  receipts: any[] = [];

  maintenance: string = "No";
  profile: number = 0;

  @ViewChild('dModal') childModal: ModalDirective;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ls: LeadsService,
    private ts: ToolsService
  ) { }

  ngOnInit(): void {    
    this.route.data
      .subscribe(data => {
        this.lead = data['user'];
        if (!this.lead){
          this.router.navigate(['/tools/service-center/user-search']);
        }
      });

   this.ls.getLeadProfileComfirmed(this.lead.id).then(profile => {
      this.profile = profile;
    });   

    this.ls.getLeadTools(this.lead.id).then(tools => {
      this.tools = tools;            
    }); 

    this.ls.getLeadActions(this.lead.id).then(actions => {
      this.actions = actions;            
    }); 
  }

  onBack(): void {
    this.router.navigate(['/tools/service-center/user-search']);
  }

  showImages(tool_id: number): void {
    this.ts.getToolReceipts(tool_id).then( data => {
      this.receipts = data;

      this.showChildModal();
    });
  }

  showChildModal(): void {
    this.childModal.show();
  }

  hideChildModal(): void {
    this.childModal.hide();
  }

  maintenanceClick() : void {
    this.ls.updateLead(this.lead).then(response => {
    });
  }

}
