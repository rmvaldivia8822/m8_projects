import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Ng2SmartTableModule } from '../../ng2-smart-table';

import { Usersearch } from './usersearch.component';
import { routing }       from './usersearch.routing';

import { ModalModule } from 'ng2-bootstrap';

import { LeadDetailGuard } from '../tables/components/leadTables/lead-detail/lead-detail-guard.services';
import { LeadDetailsResolver } from '../tables/components/leadTables/lead-detail/lead-detail-resolver.services';

import { UsersearchDetailComponent } from './usersearch-detail/usersearch-detail.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
    Ng2SmartTableModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    Usersearch,
    UsersearchDetailComponent,
  ],
  providers: [
    LeadDetailGuard,
    LeadDetailsResolver,
  ]
})
export class UsersearchModule {}
