import { Routes, RouterModule }  from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';

import { LoginGuard } from './login/login-guard.services';
import { LoginProfilesGuard } from './login/login-profile-guard.services';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/pages/login/login.module#LoginModule'
  },
  {
    path: 'tools/service-center',
    canActivate: [LoginProfilesGuard],
    loadChildren: 'app/pages/usersearch/usersearch.module#UsersearchModule'
  },
  {
    path: 'pages',
    canActivate: [LoginGuard],
    component: Pages,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule' },
      { path: 'tables', loadChildren: 'app/pages/tables/tables.module#TablesModule' }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
