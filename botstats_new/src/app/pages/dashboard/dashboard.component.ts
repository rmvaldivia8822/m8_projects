import {Component} from '@angular/core';

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})
export class Dashboard {

  stat: string = 'total';

  constructor() {
  }

  changeStat (stat: string): void {
    this.stat = stat;
  }

}
