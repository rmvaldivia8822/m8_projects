import { Component, Input } from '@angular/core';

import { MyChartistJsService } from './mychartistJs.service';
import 'style-loader!./mychartistJs.scss';


@Component({
  selector: 'mychartist-js',
  templateUrl: './mychartistJs.html',
})

export class MyChartistJs {

  data: any;
  @Input() stat: string;

  constructor(private _myChartistJsService: MyChartistJsService) {
    this.data = this._myChartistJsService.getAll();
  }

  getResponsive(padding, offset) {
    return this._myChartistJsService.getResponsive(padding, offset);
  }
}
