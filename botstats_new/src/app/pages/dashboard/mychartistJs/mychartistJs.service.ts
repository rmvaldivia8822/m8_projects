import { Injectable } from '@angular/core';

import { BaThemeConfigProvider } from '../../../theme';

import * as Chartist from 'chartist';

require('chartist-plugin-legend');

@Injectable()
export class MyChartistJsService {

  private _data = {
    statsOptions: {
      fullWidth: true,
      height: '300px',
      chartPadding: {
        right: 40,
        bottom: 40
      },
      plugins: [
          Chartist.plugins.legend({
              legendNames: ['New Leads', 'Tools', 'Referral', 'Actions'],
              clickable: true,
              position: 'bottom'
          })
      ]
    }
  };

  constructor (
    private _baConfig: BaThemeConfigProvider) { }

  public getAll() {
    return this._data;
  }

  public getResponsive(padding, offset) {
    return [
      ['screen and (min-width: 1550px)', {
        chartPadding: padding,
        labelOffset: offset,
        labelDirection: 'neutral',
        labelInterpolationFnc: function (value) {
          return value;
        }
      }],
      ['screen and (max-width: 1200px)', {
        chartPadding: padding,
        labelOffset: offset,
        labelDirection: 'neutral',
        labelInterpolationFnc: function (value) {
          return value;
        }
      }],
      ['screen and (max-width: 600px)', {
        chartPadding: 0,
        labelOffset: 0,
        labelInterpolationFnc: function (value) {
          return value[0];
        }
      }]
    ];
  }
}
