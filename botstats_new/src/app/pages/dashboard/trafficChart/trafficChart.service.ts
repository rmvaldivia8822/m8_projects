import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';



@Injectable()
export class TrafficChartService {

  constructor(
    private _baConfig:BaThemeConfigProvider) {
  }


  getData() {
    let dashboardColors = this._baConfig.get().colors.dashboard;
    return [
      {
        value: 0,
        color: dashboardColors.leads,
        highlight: colorHelper.shade(dashboardColors.leads, 15),
        label: '',
        percentage: 0,
        order: 1,
      }, {
        value: 0,
        color: dashboardColors.tools,
        highlight: colorHelper.shade(dashboardColors.tools, 15),
        label: '',
        percentage: 0,
        order: 4,
      }, {
        value: 0,
        color: dashboardColors.profiles,
        highlight: colorHelper.shade(dashboardColors.profiles, 15),
        label: '',
        percentage: 0,
        order: 3,
      }, {
        value: 0,
        color: dashboardColors.research,
        highlight: colorHelper.shade(dashboardColors.research, 15),
        label: '',
        percentage: 0,
        order: 2,
      }, {
        value: 0,
        color: dashboardColors.promotion,
        highlight: colorHelper.shade(dashboardColors.promotion, 15),
        label: '',
        percentage: 0,
        order: 0,
      },
    ];
  }
}
