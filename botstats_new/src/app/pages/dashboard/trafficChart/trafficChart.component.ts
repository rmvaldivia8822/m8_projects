import {Component} from '@angular/core';

import { LeadsService } from '../../../services/leads.service';
import { environment } from '../../../../environments/environment';

import {TrafficChartService} from './trafficChart.service';
import * as Chart from 'chart.js';

import 'style-loader!./trafficChart.scss';

@Component({
  selector: 'traffic-chart',
  templateUrl: './trafficChart.html'
})

// TODO: move chart.js to it's own component
export class TrafficChart {

  leads_total: number = 0;
  promotions: number = 0;
  product_research: number = 0;
  profiles_completed: number = 0;
  tools_registred: number = 0;
  leads_rest: number = 0;

  public doughnutData: Array<Object>;

  constructor(
    private trafficChartService:TrafficChartService,
    protected service: LeadsService) {

    this.doughnutData = trafficChartService.getData();
    
    this.service.getLeadsToPdf("").then(data => {

      this.leads_total = data.length;
      
      for ( let i=0; i<this.leads_total; i++ ) {

        if ( data[i].optin_promotions ) {
            this.promotions++;
        }
        if ( data[i].optin_product_research ) {
            this.product_research++;
        }

        this.service.getLeadProfileComfirmed(data[i].id).then(profile => {
          if ( profile )
            this.profiles_completed++;
        });

        this.service.getLeadTools(data[i].id).then(tools => {
          if ( tools.length > 0 )
            this.tools_registred++;

          if ( this.leads_total-1 == i  ) {
            this.leads_rest = (this.promotions+ this.product_research + this.profiles_completed + this.tools_registred) - this.leads_total;
            
            this.doughnutData[0]['value'] = this.leads_rest;
            this.doughnutData[0]['label'] = "Other";
            this.doughnutData[0]['percentage'] = Math.round((this.leads_rest * 100) / this.leads_total);
            
            this.doughnutData[1]['value'] = this.promotions;
            this.doughnutData[1]['label'] = "Promotions";
            this.doughnutData[1]['percentage'] = Math.round((this.promotions * 100) / this.leads_total);
            
            this.doughnutData[2]['value'] = this.product_research;
            this.doughnutData[2]['label'] = "Product Research";
            this.doughnutData[2]['percentage'] = Math.round((this.product_research * 100) / this.leads_total);
            
            this.doughnutData[3]['value'] = this.profiles_completed;
            this.doughnutData[3]['label'] = "Profile Completed";
            this.doughnutData[3]['percentage'] = Math.round((this.profiles_completed * 100) / this.leads_total);
            
            this.doughnutData[4]['value'] = this.tools_registred;
            this.doughnutData[4]['label'] = "Registred Tools";
            this.doughnutData[4]['percentage'] = Math.round((this.tools_registred * 100) / this.leads_total);
            
            this._loadDoughnutCharts();
          } 
            
        });

      }

    });

  }

  private _loadDoughnutCharts() {
    let el = jQuery('.chart-area').get(0) as HTMLCanvasElement;
    new Chart(el.getContext('2d')).Doughnut(this.doughnutData, {
      segmentShowStroke: false,
      percentageInnerCutout : 64,
      responsive: true
    });
  }
}
