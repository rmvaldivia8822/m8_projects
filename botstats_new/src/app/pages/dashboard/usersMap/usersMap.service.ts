import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, layoutPaths} from '../../../theme';

@Injectable()
export class UsersMapService {

  constructor(
    private _baConfig:BaThemeConfigProvider
    ) { 
    }

  getData() {
      var layoutColors = this._baConfig.get().colors;

      return {
          type: 'map',
          theme: 'm8',
          zoomControl: { zoomControlEnabled: true, panControlEnabled: true },
    
          dataProvider: {
            map: 'worldLow',
            zoomLevel: 2.1,
            zoomLongitude: -70,
            zoomLatitude: -10,
            areas: []
          },
    
          areasSettings: {
            rollOverOutlineColor: layoutColors.border,
            rollOverColor: layoutColors.primaryDark,
            alpha: 0.8,
            unlistedAreasAlpha: 0.2,
            unlistedAreasColor: layoutColors.primaryDark,
            balloonText: '[[title]]: [[customData]] leads'
          },
    
    
          legend: {
            width: '100%',
            marginRight: 27,
            marginLeft: 27,
            equalWidths: false,
            backgroundAlpha: 0.3,
            backgroundColor: layoutColors.border,
            borderColor: layoutColors.border,
            borderAlpha: 1,
            top: 362,
            left: 0,
            horizontalGap: 10,
            data: [
              {
                title: 'over 1 000 leads',
                color: layoutColors.primary
              },
              {
                title: '500 - 1 000 leads',
                color: layoutColors.successLight
              },
              {
                title: '100 - 500 leads',
                color: layoutColors.success
              },
              {
                title: '0 - 100 leads',
                color: layoutColors.danger
              }
            ]
          },
          export: {
            enabled: true
          },
          creditsPosition: 'bottom-right',
          pathToImages: layoutPaths.images.amChart
        };
    
  }
}
