import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

import { TotalStat } from './totalstats';

@Injectable()
export class BotstatsService {

  private statsUrl = `${environment.wepa_url}api/stats`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getTotalStats(): Promise<TotalStat[]> {
    return this.http
      .get(this.statsUrl, {headers: this.headers})
      .toPromise()
      .then(response => response.json().data as TotalStat[])
      .catch(this.handleError);
  }

  getWeeklyStatsByDay(): Promise<any> {
    return this.http
      .get(`${this.statsUrl}/thisWeek/byDay`, {headers: this.headers})
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getWeeklyStats(): Promise<any> {
    return this.http
      .get(`${this.statsUrl}/thisWeek`, {headers: this.headers})
      .toPromise()
      .then(response => response.json().data)
      .catch(this.handleError);
  }

  getTotalStatsByDay(): Promise<any> {
    return this.http
      .get(`${this.statsUrl}/totals/byDay`, {headers: this.headers})
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }  

}
