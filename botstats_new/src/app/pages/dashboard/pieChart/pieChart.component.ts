import { Component, Input } from '@angular/core';

import { PieChartService } from './pieChart.service';

import 'easy-pie-chart/dist/jquery.easypiechart.js';
import 'style-loader!./pieChart.scss';

@Component({
  selector: 'pie-chart',
  templateUrl: './pieChart.html'
})
// TODO: move easypiechart to component
export class PieChart {
  public charts: Array<Object>;
  private _init = false;
  @Input() stat: string;

  constructor(
    private _pieChartService: PieChartService
  ) { }

  ngOnChanges(): void {
    this.charts = this._pieChartService.getData(this.stat);
  }

  ngAfterViewInit() {

    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {

    jQuery('.chart').each(function () {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
          jQuery(this.el).find('.percent').text(Math.round(percent));
        },
        barColor: jQuery(this).attr('data-rel'),
        trackColor: 'rgba(0,0,0,0)',
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: 'round',
      });
    });
  }

  private _updatePieCharts() {
    jQuery('.pie-charts .card').each(function(index, chart) {
      switch(index) {
        case 0:
            jQuery(chart).addClass('leads-card');
            break;
        case 1:
            jQuery(chart).addClass('tools-card');
            break;
        case 2:
            jQuery(chart).addClass('referrals-card');
            break;
        case 3:
            jQuery(chart).addClass('actions-card');
            break;        
        default:
            jQuery(chart).addClass('none-card');
      }
    });
  }
}
