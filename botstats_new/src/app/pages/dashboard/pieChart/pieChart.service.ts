import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

import { BotstatsService } from '../dashboard.sevices';

@Injectable()
export class PieChartService {

  data: any[] = [];

  constructor(
    private _baConfig:BaThemeConfigProvider, 
    private bs: BotstatsService) {
    
    let pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
    this.data = [
      {
        color: pieColor,
        description: 'Leads',
        stats: '',
        icon: 'users',
      }, {
        color: pieColor,
        description: 'Tools',
        stats: '',
        icon: 'wrench',
      }, {
        color: pieColor,
        description: 'Referrals',
        stats: '',
        icon: 'exchange',
      }, {
        color: pieColor,
        description: 'Actions',
        stats: '',
        icon: 'line-chart',
      }
    ];

    this.getTotals();
  }

  getData(stat:string) { 
    if ( stat == 'total' ) {
      this.getTotals();
      return this.data;
    }
    else {
      this.getWeek();
      return this.data;   
    }
  }

  getTotals () {
    this.bs.getTotalStats()
      .then(stats => {
        let leads = stats['leads'].total;
        let tools = stats['tools'].total;
        let referrals = stats['referrals'].total;
        let actions = stats['actions'].total;

        this.data[0].stats = leads;
        this.data[1].stats = tools;
        this.data[2].stats = referrals;
        this.data[3].stats = actions;

        return this.data;
      }); 
  }

  getWeek () {
    this.bs.getWeeklyStats()
      .then(stats => {
        let leads = stats['leads'].total;
        let tools = stats['tools'].total;
        let referrals = stats['referrals'].total;
        let actions = stats['actions'].total;

        this.data[0].stats = leads;
        this.data[1].stats = tools;
        this.data[2].stats = referrals;
        this.data[3].stats = actions;

        return this.data;
      }); 
  }
}
