import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';

import { Pages } from './pages.component';

import { LoginGuard } from './login/login-guard.services';
import { LoginProfilesGuard } from './login/login-profile-guard.services';

@NgModule({
  imports: [
    CommonModule, 
    NgaModule, 
    routing,
  ],
  declarations: [Pages],
  providers: [
    LoginGuard,
    LoginProfilesGuard,
  ]
})
export class PagesModule {
}
