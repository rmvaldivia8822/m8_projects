export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Overview',
            icon: 'ion-android-home',
            selected: true,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'tables',
        data: {
          menu: {
            title: 'Tables',
            icon: 'ion-grid',
            selected: false,
            expanded: false,
            order: 500
          }
        },
        children: [
          {
            path: 'leads',
            data: {
              menu: {
                title: 'Leads',
                icon: 'ion-person'
              }
            }
          },
          {
            path: 'services-centers',
            data: {
              menu: {
                title: 'Service Centers',
                icon: 'ion-ios-medkit'
              }
            }
          },
          {
            path: 'distributors',
            data: {
              menu: {
                title: 'Distributors',
                icon: 'ion-ios-cart'
              }
            }
          },
          {
            path: 'research',
            data: {
              menu: {
                title: 'Research',
                icon: 'ion-help'
              }
            }
          },
          {
            path: 'actions',
            data: {
              menu: {
                title: 'Actions',
                icon: 'ion-podium'
              }
            }
          }

        ]
      },      
      {
        path: '',
        data: {
          menu: {
            title: 'Logout',
            url: '/login',
            icon: 'ion-android-exit',
            order: 800
          }
        }
      },

    ]
  }
];
