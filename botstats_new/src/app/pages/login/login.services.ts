import { Injectable } from '@angular/core';
import { Http , URLSearchParams , Response, Headers  } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';


@Injectable()
export class LoginService {
  private OauthLoginEndPointUrl = `${environment.wepa_url}oauth/token`;
  private clientId = environment.wepa_oauth_client_id;
  private clientSecret = environment.wepa_oauth_client_secret;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/x-www-form-urlencoded'
  });

  constructor(public http: Http) {}

  login(username, password) : Observable<any> {

    let params: URLSearchParams = new URLSearchParams();
     params.append('username', username );
     params.append('password', password );
     params.append('client_id', this.clientId );
     params.append('client_secret', this.clientSecret );
     params.append('grant_type', 'password' );


    return this.http.post(this.OauthLoginEndPointUrl , params, {
                   headers: this.headers
                 }).map(this.handleData)
                   .catch(this.handleError);
  }


  isLogin (): boolean {
    return (localStorage.getItem("token") === null) ? false : true;
  }

 
  private handleData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

  public logout() {
     localStorage.removeItem('token');
     localStorage.removeItem('refresh_token');
     localStorage.removeItem('expires_in');
     localStorage.removeItem('profile');
  }

  getUserByEmail(email: number): Promise<any[]> {
    return this.http
            .get(`${environment.wepa_url}/api/users/search/email/${email}`, {headers: this.headers})
            .toPromise()
            .then(response => response.json().users)
            .catch(this.handleError);        
    
  }
}