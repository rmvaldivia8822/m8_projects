import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';

import { LoginService } from './login.services';

import 'style-loader!./login.scss';

@Component({
  selector: 'login',
  templateUrl: './login.html',
})
export class Login implements OnInit {

  public form: FormGroup;
  public email: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;

  private show = 'fade';

  constructor(
    fb:FormBuilder,
    public router: Router,
    private loginService: LoginService
    ) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  ngOnInit () {
    this.loginService.logout();
  }

  public onSubmit(values:Object):void {
    this.submitted = true;
    if (this.form.valid) {
      this.loginService.login(this.email.value, this.password.value)
        .subscribe(
            response => {
            this.loginService.getUserByEmail(this.email.value).then( users => {              
              if ( users.length > 0 ) {
                if ( users[0].profile == 0 ) { //Admin
                  localStorage.setItem('token', response.access_token);
                  localStorage.setItem('refresh_token', response.refresh_token);
                  localStorage.setItem('expires_in', response.expires_in);
                  localStorage.setItem('profile', users[0].profile);
                  this.router.navigateByUrl('/pages');
                }
                else if ( users[0].profile == 1 ) {  //Services Centers
                  localStorage.setItem('token', response.access_token);
                  localStorage.setItem('refresh_token', response.refresh_token);
                  localStorage.setItem('expires_in', response.expires_in);
                  localStorage.setItem('profile', users[0].profile);
                  this.router.navigateByUrl('/tools/service-center/user-search');
                } else {
                  this.show = 'show';
                }
              } 
              else {
                this.show = 'show';
              } 
            });

            
          },
          error => {
            this.show = 'show';
          }
        );
    }
  }
}
