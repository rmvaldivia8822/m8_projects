import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable()
export class LoginProfilesGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate(): boolean {
        
        if(localStorage.getItem("token") === null || (localStorage.getItem("profile") != '1' && localStorage.getItem("profile") != '0')){
          this.router.navigateByUrl('/login');
        }        
        return (localStorage.getItem("token") === null || (localStorage.getItem("profile") != '1' && localStorage.getItem("profile") != '0')) ? false : true;
    }
}