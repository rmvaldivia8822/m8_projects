import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate(): boolean {
        
        if(localStorage.getItem("token") === null || localStorage.getItem("profile") != '0'){
          this.router.navigateByUrl('/login');
        }        
        return (localStorage.getItem("token") === null || localStorage.getItem("profile") != '0' ) ? false : true;
    }
}