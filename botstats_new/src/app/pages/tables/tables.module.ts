import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { Ng2SmartTableModule } from '../../ng2-smart-table';

import { routing }       from './tables.routing';
import { Tables } from './tables.component';

import { LeadTables } from './components/leadTables/leadTables.component';

import { LeadDetailComponent } from './components/leadTables/lead-detail/lead-detail.component';
import { LeadDetailGuard } from './components/leadTables/lead-detail/lead-detail-guard.services';
import { LeadDetailsResolver } from './components/leadTables/lead-detail/lead-detail-resolver.services';

import { ServiceCentersTables } from './components/serviceCentersTables/serviceCentersTables.component';

import { ServiceCentersDetailComponent } from './components/serviceCentersTables/serviceCenters-detail/serviceCenters-detail.component';
import { ServiceCentersDetailGuard } from './components/serviceCentersTables/serviceCenters-detail/serviceCenters-guard.services';
import { ServiceCentersDetailsResolver } from './components/serviceCentersTables/serviceCenters-detail/serviceCenters-resolver.services';

import { ServiceCentersEditComponent } from './components/serviceCentersTables/serviceCenters-edit/serviceCenters-edit.component';


import { DistributorsTables } from './components/distributorsTables/distributorsTables.component';

import { DistributorsDetailComponent } from './components/distributorsTables/distributors-detail/distributors-detail.component';
import { DistributorsDetailGuard } from './components/distributorsTables/distributors-detail/distributors-guard.services';
import { DistributorsDetailsResolver } from './components/distributorsTables/distributors-detail/distributors-resolver.services';

import { DistributorsEditComponent } from './components/distributorsTables/distributors-edit/distributors-edit.component';

import { ActionsTables } from './components/actionsTables/actionsTables.component';

import { ResearchTables } from './components/researchTables/researchTables.component';

import { ResearchDetailComponent } from './components/researchTables/research-detail/research-detail.component';
import { ResearchDetailGuard } from './components/researchTables/research-detail/research-detail-guard.services';
import { ResearchDetailsResolver } from './components/researchTables/research-detail/research-detail-resolver.services';


import { LeadStart } from './components/leadTables/leadStart/leadStart.component';

import { AgmCoreModule } from 'angular2-google-maps/core';

import { environment } from '../../../environments/environment';

import { ModalModule } from 'ng2-bootstrap';

import {TooltipModule} from "ngx-tooltip";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    routing,
    Ng2SmartTableModule,
    AgmCoreModule.forRoot({
      apiKey: environment.google_maps,
      libraries: ["places"]
    }),
    ModalModule.forRoot(),
    TooltipModule,
  ],
  declarations: [
    Tables,
    LeadTables,
    LeadDetailComponent,
    ServiceCentersTables,
    ServiceCentersDetailComponent,
    ServiceCentersEditComponent,
    DistributorsTables,
    DistributorsDetailComponent,
    DistributorsEditComponent,
    LeadStart,
    ActionsTables,
    ResearchTables,
    ResearchDetailComponent,
  ],
  providers: [
    LeadDetailGuard,
    LeadDetailsResolver,
    ServiceCentersDetailGuard,
    ServiceCentersDetailsResolver,
    DistributorsDetailGuard,
    DistributorsDetailsResolver,
    ResearchDetailGuard,
    ResearchDetailsResolver,
  ],
  entryComponents: [
    LeadStart,
  ]
})
export class TablesModule {
}
