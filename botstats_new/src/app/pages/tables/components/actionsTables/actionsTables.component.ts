import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';

import { Router } from '@angular/router';

import { ServerDataSource } from '../../../../ng2-smart-table';

import { ActionsServices } from '../../../../services/actions.services';

import { environment } from '../../../../../environments/environment';

import 'style-loader!./actionsTables.scss';

import { ModalDirective } from 'ng2-bootstrap';

declare var jsPDF: any; // Important

@Component({
  selector: 'actions-tables',
  templateUrl: './actionsTables.html',
})
export class ActionsTables {
  @ViewChild('dModal') childModal: ModalDirective;

  remove = null;
  query: string = '';
  
  welcome:number = 0;
  stores:number = 0;
  services:number = 0;
  register:number = 0;
  regstep2:number = 0;
  regstep1:number = 0;
  regstep3:number = 0;
  registerok:number = 0;
  register2:number = 0;

  emailok:number = 0;
  emailko:number = 0;

  invstep1:number = 0;
  daterecognized:number = 0;
  invstep4:number = 0;

  uplstep1:number = 0;
  uplok:number = 0;
  uplko:number = 0;

  research:number = 0;
  researchok:number = 0;
  researchko:number = 0;
  endresearch:number = 0;

  source: ServerDataSource;

  constructor(
    protected service: ActionsServices,
    private router: Router,
    http: Http
  ) {
    
  }

  ngOnInit(){

    this.service.getTotalsActions().then(data => {
      
      if (typeof(data) === 'object') {
          
          this.welcome = data.WELCOME.action_total;

          this.stores = data.STORES.action_total;

          this.services = data.SERVICE.action_total;          

          this.register = data.REGISTER.action_total;          

          this.regstep1 = data.REGSTEP1.action_total;          
          this.regstep2 = data.REGSTEP2.action_total;          
          this.regstep3 = data.REGSTEP3.action_total;          

          this.registerok = data.REGISTEROK.action_total;          
          this.register2 = data.REGISTER2.action_total;          

          this.emailok = data.EMAILOK.action_total;          
          this.emailko = data.EMAILKO.action_total;     

          this.invstep1 = data.INVSTEP1.action_total;     
          this.daterecognized = data.DATERECOGNIZED.action_total;     
          this.invstep4 = data.INVSTEP4.action_total;     

          this.uplstep1 = data.UPLSTEP1.action_total;     
          this.uplok = data.UPLOK.action_total;     
          this.uplko = data.UPLKO.action_total;    

          this.research = data.RESEARCH.action_total;    
          this.researchok = data.RESEARCHOK.action_total;    
          this.researchko = data.RESEARCHKO.action_total;    
          this.endresearch = data.ENDRESEARCH.action_total;    
      }

      /*
      new Chartist.Bar('.ct-chart', {
        labels: ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL'],
        series: [20, 60, 120, 200, 180, 20, 10]
      }, {
        distributeSeries: true
      });      
      */

    });

  }

}
