import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Lead } from '../../../../../lead';
import { Tool } from '../../../../../tool';
import { Action } from '../../../../../action';

import { LeadsService } from '../../../../../services/leads.service';

@Component({
  selector: 'app-lead-details',
  templateUrl: './lead-detail.html',
  styleUrls: ['./lead-detail.scss']
})
export class LeadDetailComponent implements OnInit {
  lead: Lead;
  tools: Tool[] = [];
  actions: Action[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ls: LeadsService
  ) { }

  ngOnInit(): void {    
    this.route.data
      .subscribe(data => {
        this.lead = data['user'];
        //console.log("Lead: " + JSON.stringify(this.lead));
        if (!this.lead){
          this.router.navigate(['/pages/tables/leads']);
        }
      });

    this.ls.getLeadTools(this.lead.id).then(tools => {
      this.tools = tools;            
    }); 

    this.ls.getLeadActions(this.lead.id).then(actions => {
      this.actions = actions;            
    }); 
  }

  onBack(): void {
    this.router.navigate(['/pages/tables/leads']);
  }

}
