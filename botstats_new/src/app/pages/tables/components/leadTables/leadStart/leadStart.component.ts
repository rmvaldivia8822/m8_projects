import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from '../../../../../ng2-smart-table';

import { LeadsService } from '../../../../../services/leads.service';

@Component({
  selector: 'lead-start',
  templateUrl: './leadStart.html',
  styleUrls: ['./leadStart.scss']
})
export class LeadStart implements ViewCell, OnInit {

  @Input() value: number;
  optin_promotions: number = 0;
  optin_product_research: number = 0;
  tools: any = [];
  profile: number = 0;

  loading: boolean = true;

  constructor(protected service: LeadsService) {
  }

  ngOnInit() {
    this.service.getLead(this.value).then(data => {
      this.optin_promotions = data.optin_promotions;
      this.optin_product_research = data.optin_product_research;
    });
    this.service.getLeadProfileComfirmed(this.value).then(data => {
      this.profile = data;
    });
    this.service.getLeadTools(this.value).then(data => {
      this.tools = data;
      this.loading = false;
    });
  }

}


