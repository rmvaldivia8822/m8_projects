import { Component } from '@angular/core';
import { Http } from '@angular/http';

import { Router } from '@angular/router';

import { ServerDataSource } from '../../../../ng2-smart-table';

import { LeadsService } from '../../../../services/leads.service';

import { environment } from '../../../../../environments/environment';

import { LeadStart } from './leadStart/leadStart.component';

import { Lead } from '../../../../lead';

import 'style-loader!./leadTables.scss';

declare var jsPDF: any; // Important

@Component({
  selector: 'lead-tables',
  templateUrl: './leadTables.html',
})
export class LeadTables {

  query: string = '';

  settings = {
    actions: null,
    pager: {
      perPage: 10
    },
    columns: {
      name: {
        title: 'Name',
        type: 'html',
        filter: true,
        valuePrepareFunction: (name) => {
          if (name.length <= 1) {
            return '<b class="text-rows">' + 'Unknown' + "</b>";
          }
          return '<b class="text-rows">' + name + "</b>" ;
        }
      },
      country: {
        title: 'From',
        type: 'html',
        filter: true,
        valuePrepareFunction: (country) => {
          if (!country)
            return 'N/A';

          return country;
        }
      },
      id: {
        title: 'Opt-ins',
        type: 'custom',
        filter: false,
        renderComponent: LeadStart
      }
    }
  };

  source: ServerDataSource;

  constructor(protected service: LeadsService, private router: Router, http: Http) {
      this.source = new ServerDataSource(http, {
        endPoint: `${environment.wepa_url}api/leads`,
        dataKey: 'leads.data',
        totalKey: 'leads.total',
        pagerPageKey: 'page',
        pagerLimitKey: 'limit',
        filterFieldKey: '#field#'
      });
    }

    onUserRowSelect(event): void {
      this.router.navigate(['pages/tables/leads/'+event.data.id]);
    }

    download(): void {

      let doc = new jsPDF('l');
      this.query = localStorage.getItem("search");

      let columns = [
          {title: "ID", dataKey: "id"},
          {title: "Name", dataKey: "name"}, 
          {title: "Country", dataKey: "country"},
          {title: "Address", dataKey: "address"},
          {title: "City", dataKey: "city"},
          {title: "State", dataKey: "state"},
          {title: "Phone", dataKey: "phone"},
          {title: "Zip", dataKey: "zipcode"}
      ];
      let rows = [];

      this.service.getLeadsToPdf(this.query).then(data => {

        for (let i=0; i<data.length; i++) {
          let name = "";
          let country = "";
          let address = "";
          let city = "";
          let state = "";
          let phone = "";
          let zipcode = "";
          
          if ( data[i]['name'] )
            name = data[i]['name'];
          if ( data[i]['country'] )
            country = data[i]['country'];
          if ( data[i]['address'] )
            address = data[i]['address'];
          if ( data[i]['city'] )
            city = data[i]['city'];
          if ( data[i]['state'] )
            state = data[i]['state'];
          if ( data[i]['phone'] )
            phone = data[i]['phone'];
          if ( data[i]['zipcode'] )
            zipcode = data[i]['zipcode'];        

          
          let row = {
            "id": data[i]['id'], 
            "name": name, 
            "country": country,
            "address": address,
            "city": city,
            "state": state,
            "phone": phone,
            "zipcode": zipcode,
          };
          rows.push (row);
        }

        let msg = "This information is private and its use and distribution must be restricted only to persons who have the appropriate authorization.";

        doc.setFontSize(18);
        doc.text('Leads Table', 14, 22);
        doc.setFontSize(9);
        doc.setTextColor(170, 57, 57);
        var text = doc.splitTextToSize(msg, doc.internal.pageSize.width - 35, {});
        doc.text(text, 14, 30);

        doc.autoTable(columns, rows, {
            startY: 40,
            showHeader: 'everyPage',
            bodyStyles: {valign: 'top'},
            styles: {overflow: 'linebreak', columnWidth: 'wrap'},
            columnStyles: {
              name: {columnWidth: 'auto'},
              address: {columnWidth: 'auto'},
              city: {columnWidth: 'auto'},
              state: {columnWidth: 'auto'},
              phone: {columnWidth: 'auto'},
              zipcode: {columnWidth: 'auto'}
            }
        });
        
        doc.save("leads.pdf");

      });
      
    }


  }


