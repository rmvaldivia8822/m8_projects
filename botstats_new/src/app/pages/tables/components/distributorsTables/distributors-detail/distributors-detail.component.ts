import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Distributors } from '../../../../../distributors';

@Component({
  selector: 'distributors-details',
  templateUrl: './distributors-detail.html',
  styleUrls: ['./distributors-detail.scss']
})
export class DistributorsDetailComponent implements OnInit {
  distributor: Distributors;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {    
    this.route.data
      .subscribe(data => {
        this.distributor = data['distributor'];  
        if (!this.distributor){
          this.router.navigate(['/pages/tables/distributors']);
        }
      });
  }

  onBack(): void {
    this.router.navigate(['/pages/tables/distributors']);
  }

}
