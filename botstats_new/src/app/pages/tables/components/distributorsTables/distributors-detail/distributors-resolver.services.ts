import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Distributors } from '../../../../../distributors';

import { DistributorsService } from '../../../../../services/distributors.services';


@Injectable()
export class DistributorsDetailsResolver implements Resolve<any> {

  constructor(
    private ds: DistributorsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Distributors> {
    return this.ds.getDistributor(route.params['id'])
      .then(distributor => {
        if (distributor) {            
          distributor.latitude = +distributor.latitude; 
          distributor.longitude = +distributor.longitude; 
          
          return distributor;
        }
        else {
          this.router.navigate(['/pages/tables/distributors']);
        } 
      });
  }
}