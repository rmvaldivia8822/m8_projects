import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable()
export class ResearchDetailGuard implements CanActivate{

    constructor(
        private _router: Router) {                
    }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        
        let id = +route.url[1].path;
        //alert("Id: " + id);
        if (isNaN(id) || id < 1) {
            //alert('Invalid lead Id');
            // start a new navigation to redirect to list page
            this._router.navigate(['/pages/tables/research']);
            // abort current navigation
            return false;
        } 

        return true;
    }
}