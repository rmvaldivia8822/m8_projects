import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { Lead } from '../../../../../lead';

import { LeadsService } from '../../../../../services/leads.service';

@Injectable()
export class ResearchDetailsResolver implements Resolve<any> {

  constructor(
    private ls: LeadsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Lead> {
    return this.ls.getLead(route.params['id'])
      .then(lead => {
        if (lead)
          return lead;
        else {
          this.router.navigate(['/pages/tables/research']);
        } 
      });
  }
}