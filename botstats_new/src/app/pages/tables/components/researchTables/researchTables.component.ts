import { Component } from '@angular/core';
import { Http } from '@angular/http';

import { Router } from '@angular/router';

import { ServerDataSource } from '../../../../ng2-smart-table';

import { LeadsService } from '../../../../services/leads.service';
import { ProfilesService } from '../../../../services/profiles.services';

import { environment } from '../../../../../environments/environment';

import { Lead } from '../../../../lead';
import { Profile } from '../../../../profile';

import 'style-loader!./researchTables.scss';

declare var jsPDF: any; // Important

@Component({
  selector: 'research-tables',
  templateUrl: './researchTables.html',
})
export class ResearchTables {

  query: string = '';

  settings = {
    actions: null,
    pager: {
      perPage: 10
    },
    columns: {
      name: {
        title: 'Name',
        type: 'html',
        filter: true,
        valuePrepareFunction: (name) => {
          if (name.length <= 1) {
            return '<b>' + 'Unknown' + "</b>";
          }
          return '<b>' + name + "</b>" ;
        }
      },
      research: {
        title: 'Questions & Answers',
        type: 'html',
        filter: true,
        valuePrepareFunction: (research) => {
          if (!research)
            return 'N/A';

          let res = JSON.parse(research);
          let response = [];
          let html = "<ul>";

          for (var key in res) {
              if (!res.hasOwnProperty(key)) continue;
              response.push(res[key]);
          }
          for ( let i=0; i<response.length; i++ ) {
            if ( response[i].answer != "" ) {
                html += "<li><b>"+this.fixingQuestions(response[i].question)+": </b>"+response[i].answer+"</li>";
            }
          }
          return html + "</ul>";
        }
      }
    }
  };

  source: ServerDataSource;

  fixingQuestions ( question: string ) {
      if ( question.charAt(0) != "¿" )
        question = "¿" + question;
      if ( question.charAt(-1) != "?" || question.charAt(-1) == ":")
        question = question.substring(0, question.length-1) + "?";  

      return question;  
  }

  constructor(
    protected service: ProfilesService, 
    private router: Router, 
    http: Http,
    private ls: ProfilesService) {
      this.source = new ServerDataSource(http, {
        endPoint: `${environment.wepa_url}api/profiles`,
        dataKey: 'profiles.data',
        totalKey: 'profiles.total',
        pagerPageKey: 'page',
        pagerLimitKey: 'limit',
        filterFieldKey: '#field#'
      });
    }

    download(): void {

      let doc = new jsPDF('l');
      this.query = localStorage.getItem("search");

      let columns = [
          {title: "ID", dataKey: "id"},
          {title: "Name", dataKey: "name"}, 
          {title: "Country", dataKey: "country"},
          {title: "Phone", dataKey: "phone"},
          {title: "Questions & Answers", dataKey: "research"}
      ];
      let rows = [];

      this.service.getProfilesToPdf(this.query).then(data => {

        for (let i=0; i<data.length; i++) {
          let name = "";
          let country = "";
          let phone = "";
          let research = "";
          
          if ( data[i]['name'] )
            name = data[i]['name'];
          if ( data[i]['country'] )
            country = data[i]['country'];
          if ( data[i]['phone'] )
            phone = data[i]['phone'];
          if ( data[i]['research'] ) {
            research = data[i]['research'];
            
            let res = JSON.parse(research);
            let response = [];
            let pair = "";
  
            for (var key in res) {
                if (!res.hasOwnProperty(key)) continue;
                response.push(res[key]);
            }
            for ( let i=0; i<response.length; i++ ) {
              if ( response[i].answer != "" ) 
                  pair += this.fixingQuestions(response[i].question)+": "+response[i].answer + ", ";
              
            }
            research = pair;
          }
          
          let row = {
            "id": data[i]['id'], 
            "name": name, 
            "country": country,
            "phone": phone,
            "research": research,
          };
          rows.push (row);
        }

        let msg = "This information is private and its use and distribution must be restricted only to persons who have the appropriate authorization.";

        doc.setFontSize(18);
        doc.text('Questions & Answers Table', 14, 22);
        doc.setFontSize(9);
        doc.setTextColor(170, 57, 57);
        var text = doc.splitTextToSize(msg, doc.internal.pageSize.width - 35, {});
        doc.text(text, 14, 30);

        doc.autoTable(columns, rows, {
            startY: 40,
            showHeader: 'everyPage',
            bodyStyles: {valign: 'top'},
            styles: {overflow: 'linebreak', columnWidth: 'wrap'},
            columnStyles: {
              name: {columnWidth: 'auto'},
              phone: {columnWidth: 25},
              research: {columnWidth: 'auto'}
            }
        });
        
        doc.save("research.pdf");

      });
      
    }


  }


