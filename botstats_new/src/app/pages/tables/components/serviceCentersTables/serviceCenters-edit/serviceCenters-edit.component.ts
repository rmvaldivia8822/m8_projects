import { Component, OnInit, Input, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormControl, FormGroup, AbstractControl, FormBuilder, Validators } from "@angular/forms";
import { MapsAPILoader } from 'angular2-google-maps/core';


import { ServiceCenters } from '../../../../../servicecenter';

import { ServiceCentersService } from '../../../../../services/servicescenters.service';

declare var google:any;

@Component({
  selector: 'servicecenter-edit',
  templateUrl: './serviceCenters-edit.html',
  styleUrls: ['./serviceCenters-edit.scss']
})
export class ServiceCentersEditComponent implements OnInit {
  public serviceCenter: ServiceCenters;
  create = false;

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;


  public form:FormGroup;
  public name:AbstractControl;
  public phone:AbstractControl;
  public website_url:AbstractControl;
  public google_maps_url:AbstractControl;
  public address:AbstractControl;
  public city:AbstractControl;
  public state:AbstractControl;
  public province:AbstractControl;
  public country:AbstractControl;
  public zipcode:AbstractControl;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private scs: ServiceCentersService,
    fb:FormBuilder
  ) { 
    this.form = fb.group({
      'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'phone': ['', Validators.compose([Validators.required, Validators.minLength(9)])],
      'website_url': ['', Validators.compose([Validators.minLength(4)])],
      'google_maps_url': ['', Validators.compose([Validators.minLength(4)])],
      'address': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'city': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'state': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'province': ['', Validators.compose([Validators.minLength(4)])],
      'country': ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      'zipcode': ['', Validators.compose([Validators.minLength(4)])]

    });

    this.name = this.form.controls['name'];
    this.phone = this.form.controls['phone'];
    this.website_url = this.form.controls['website_url'];
    this.google_maps_url = this.form.controls['google_maps_url'];
    this.address = this.form.controls['address'];
    this.city = this.form.controls['city'];
    this.state = this.form.controls['state'];
    this.province = this.form.controls['province'];
    this.country = this.form.controls['country'];
    this.zipcode = this.form.controls['zipcode'];

  }

  ngOnInit(): void {    
    this.route.data
      .subscribe(data => {
        this.serviceCenter = data['serviceCenter'];    
        //console.log("Service Center: "+ JSON.stringify(this.serviceCenter));    
        if (!this.serviceCenter){
          //this.router.navigate(['/pages/tables/services-centers']);
          this.create = true;
          this.serviceCenter = new ServiceCenters();
        } 

        this.zoom = 8;
        //Miami Offices lat: 25.7987187 lng: -80.18950159999997        
        this.latitude = (this.serviceCenter.latitude)?this.serviceCenter.latitude:25.7987187;
        this.longitude = (this.serviceCenter.longitude)?this.serviceCenter.longitude:-80.18950159999997;
        //console.log("Service: " + JSON.stringify(this.serviceCenter));
        this.serviceCenter.latitude = this.latitude;
        this.serviceCenter.longitude = this.longitude; 
        
        let componentForm = {
          street_number: 'short_name',
          route: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
        };
    
        //create search FormControl
        this.searchControl = new FormControl();
    
        //set current position
        this.setCurrentPosition();
    
        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
          let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
            types: ["address"]
          });
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              //get the place result
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();
    
              //verify result
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }

              this.serviceCenter.google_maps_url = place.url;
              let route = "";

              for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                  var val = place.address_components[i][componentForm[addressType]];
                  
                  if (addressType == "street_number") {
                      this.serviceCenter.address = val;
                  }

                  if (addressType == "route") {
                      route = val;
                  }

                  if (addressType == "locality") {
                      this.serviceCenter.city = val;
                  }

                  if (addressType == "administrative_area_level_1") {
                      this.serviceCenter.state = val;
                  }

                  if (addressType == "country") {
                      this.serviceCenter.country = val;
                  }

                  if (addressType == "postal_code") {
                      this.serviceCenter.zipcode = val;
                  }
                }
              }

              this.serviceCenter.address = `${this.serviceCenter.address} ${route}`;
    
              //set latitude, longitude and zoom
              this.serviceCenter.latitude = this.latitude = place.geometry.location.lat();
              this.serviceCenter.longitude = this.longitude = place.geometry.location.lng();              
              this.zoom = 8;
            });
          });
        });

      });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
      });
    }
  }

  onBack(): void {
    this.router.navigate(['/pages/tables/services-centers']);
  }

  onUpdate(serviceCenter): void {
    this.scs.updateServiceCenter(serviceCenter).then(data => {
      this.onBack();       
    });    
  }

  onCreate(serviceCenter): void {
    this.scs.createServiceCenter(serviceCenter).then(data => {
      this.onBack();       
    });    
  }

}
