import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { ServiceCenters } from '../../../../../servicecenter';

import { ServiceCentersService } from '../../../../../services/servicescenters.service';


@Injectable()
export class ServiceCentersDetailsResolver implements Resolve<any> {

  constructor(
    private sc: ServiceCentersService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<ServiceCenters> {
    return this.sc.getServiceCenter(route.params['id'])
      .then(scenter => {
        if (scenter) {            
          scenter.latitude = +scenter.latitude; 
          scenter.longitude = +scenter.longitude; 
          
          return scenter;
        }
        else {
          this.router.navigate(['/pages/tables/services-centers']);
        } 
      });
  }
}