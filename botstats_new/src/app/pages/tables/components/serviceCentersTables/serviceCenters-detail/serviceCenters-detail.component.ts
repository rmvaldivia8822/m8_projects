import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ServiceCenters } from '../../../../../servicecenter';

@Component({
  selector: 'servicecenter-details',
  templateUrl: './serviceCenters-detail.html',
  styleUrls: ['./serviceCenters-detail.scss']
})
export class ServiceCentersDetailComponent implements OnInit {
  serviceCenter: ServiceCenters;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {    
    this.route.data
      .subscribe(data => {
        this.serviceCenter = data['serviceCenter'];    
        //console.log("Service Center: "+ JSON.stringify(this.serviceCenter));    
        if (!this.serviceCenter){
          this.router.navigate(['/pages/tables/services-centers']);
        }
      });
  }

  onBack(): void {
    this.router.navigate(['/pages/tables/services-centers']);
  }

}
