import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';

import { Router } from '@angular/router';

import { ServerDataSource } from '../../../../ng2-smart-table';

import { ServiceCentersService } from '../../../../services/servicescenters.service';

import { environment } from '../../../../../environments/environment';

import 'style-loader!./serviceCentersTables.scss';

import { ModalDirective } from 'ng2-bootstrap';

declare var jsPDF: any; // Important

@Component({
  selector: 'servicesCenters-tables',
  templateUrl: './serviceCentersTables.html',
})
export class ServiceCentersTables {

  @ViewChild('dModal') childModal: ModalDirective;

  remove = null;

  query: string = '';

  settings = {
    pager: {
      perPage: 10
    },
    mode: 'external',
    add: {
      addButtonContent: '<i class="ion-ios-plus-outline button-add"></i>'
    },
    edit: {
      editButtonContent: '<i class="ion-edit button-actions"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a button-actions"></i>'
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        filter: true,
      },
      name: {
        title: 'Name',
        type: 'html',
        filter: true,
        valuePrepareFunction: (name) => {
          if (!name) {
            return '<b class="text-rows">' + 'Unknown' + "</b>";
          }
          return '<b class="text-rows">' + name + "</b>";
        }
      },
      country: {
        title: 'From',
        type: 'html',
        filter: true,
        valuePrepareFunction: (country) => {
          if (!country)
            return '<div class=""><i class="fa fa-ban fa-lg" aria-hidden="true"></i></div>';

          return "<div class=''>" + country + "</div>";
        }
      },
      phone: {
        title: 'Phone',
        type: 'string',
        filter: true,
      },
    }
  };

  source: ServerDataSource;

  constructor(protected service: ServiceCentersService, private router: Router, http: Http) {
      this.source = new ServerDataSource(http, {
        endPoint: `${environment.wepa_url}api/servicecenters`,
        dataKey: 'servicecenters.data',
        totalKey: 'servicecenters.total',
        pagerPageKey: 'page',
        pagerLimitKey: 'limit',
        filterFieldKey: '#field#'
      });
    }

    onUserRowSelect(event): void {
      this.router.navigate(['pages/tables/services-centers/' + event.data.id]);
    }

    create (event): void {
      this.router.navigate(['pages/tables/services-centers-create/']);
    }

    edit (event): void {
      this.router.navigate(['pages/tables/services-centers-edit/' + event.data.id]);
    }

    delete (event): void {
      this.remove = event.data;
      this.showChildModal();
    }

    deleteAction (): void {
      this.service.deleteServiceCenter(this.remove.id).then(data => {
        if ( !data ) {
          this.source.remove(this.remove);
          this.hideChildModal();
        }
      });
    }

    showChildModal(): void {
      this.childModal.show();
    }

    hideChildModal(): void {
      this.remove = null;
      this.childModal.hide();
    }

    download(): void {

      let doc = new jsPDF('l');
      this.query = localStorage.getItem("search");

      let columns = [
          {title: "ID", dataKey: "id"},
          {title: "Name", dataKey: "name"}, 
          {title: "Country", dataKey: "country"},
          {title: "Address", dataKey: "address"},
          {title: "City", dataKey: "city"},
          {title: "State", dataKey: "state"},
          {title: "Phone", dataKey: "phone"},
          {title: "Zip", dataKey: "zipcode"}
      ];
      let rows = [];

      this.service.getServiceCenterToPdf(this.query).then(data => {

        for (let i=0; i<data.length; i++) {
          let name = "";
          let country = "";
          let address = "";
          let city = "";
          let state = "";
          let phone = "";
          let zipcode = "";
          
          if ( data[i]['name'] )
            name = data[i]['name'];
          if ( data[i]['country'] )
            country = data[i]['country'];
          if ( data[i]['address'] )
            address = data[i]['address'];
          if ( data[i]['city'] )
            city = data[i]['city'];
          if ( data[i]['state'] )
            state = data[i]['state'];
          if ( data[i]['phone'] )
            phone = data[i]['phone'];
          if ( data[i]['zipcode'] )
            zipcode = data[i]['zipcode'];        

          
          let row = {
            "id": data[i]['id'], 
            "name": name, 
            "country": country,
            "address": address,
            "city": city,
            "state": state,
            "phone": phone,
            "zipcode": zipcode,
          };
          rows.push (row);
        }

        let msg = "This information is private and its use and distribution must be restricted only to persons who have the appropriate authorization.";

        doc.setFontSize(18);
        doc.text('Services Centers Table', 14, 22);
        doc.setFontSize(9);
        doc.setTextColor(170, 57, 57);
        var text = doc.splitTextToSize(msg, doc.internal.pageSize.width - 35, {});
        doc.text(text, 14, 30);

        doc.autoTable(columns, rows, {
            startY: 40,
            showHeader: 'everyPage',
            bodyStyles: {valign: 'top'},
            styles: {overflow: 'linebreak', columnWidth: 'wrap'},
            columnStyles: {
              name: {columnWidth: 'auto'},
              address: {columnWidth: 'auto'},
              city: {columnWidth: 'auto'},
              state: {columnWidth: 'auto'},
              phone: {columnWidth: 'auto'},
              zipcode: {columnWidth: 'auto'}
            }
        });
        
        doc.save("servicecenters.pdf");

      });
      
    }


  }


