import { Routes, RouterModule }  from '@angular/router';

import { Tables } from './tables.component';
import { LeadTables } from './components/leadTables/leadTables.component';
import { ServiceCentersTables } from './components/serviceCentersTables/serviceCentersTables.component';

import { LeadDetailComponent } from './components/leadTables/lead-detail/lead-detail.component';
import { LeadDetailGuard } from './components/leadTables/lead-detail/lead-detail-guard.services';
import { LeadDetailsResolver } from './components/leadTables/lead-detail/lead-detail-resolver.services';

import { ServiceCentersDetailComponent } from './components/serviceCentersTables/serviceCenters-detail/serviceCenters-detail.component';
import { ServiceCentersDetailGuard } from './components/serviceCentersTables/serviceCenters-detail/serviceCenters-guard.services';
import { ServiceCentersDetailsResolver } from './components/serviceCentersTables/serviceCenters-detail/serviceCenters-resolver.services';

import { ServiceCentersEditComponent } from './components/serviceCentersTables/serviceCenters-edit/serviceCenters-edit.component';


import { DistributorsTables } from './components/distributorsTables/distributorsTables.component';

import { DistributorsDetailComponent } from './components/distributorsTables/distributors-detail/distributors-detail.component';
import { DistributorsDetailGuard } from './components/distributorsTables/distributors-detail/distributors-guard.services';
import { DistributorsDetailsResolver } from './components/distributorsTables/distributors-detail/distributors-resolver.services';

import { DistributorsEditComponent } from './components/distributorsTables/distributors-edit/distributors-edit.component';

import { ActionsTables } from './components/actionsTables/actionsTables.component';

import { ResearchTables } from './components/researchTables/researchTables.component';

import { ResearchDetailComponent } from './components/researchTables/research-detail/research-detail.component';
import { ResearchDetailGuard } from './components/researchTables/research-detail/research-detail-guard.services';
import { ResearchDetailsResolver } from './components/researchTables/research-detail/research-detail-resolver.services';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Tables,
    children: [
      { path: 'leads', 
        component: LeadTables 
      },
      { path: 'leads/:id', 
        component: LeadDetailComponent,
        canActivate: [LeadDetailGuard],
        resolve: {
          user: LeadDetailsResolver
        } 
      },
      { path: 'services-centers', 
        component: ServiceCentersTables 
      },
      { path: 'services-centers/:id', 
        component: ServiceCentersDetailComponent,
        canActivate: [ServiceCentersDetailGuard],
        resolve: {
          serviceCenter: ServiceCentersDetailsResolver
        } 
      },
      { path: 'services-centers-edit/:id', 
        component: ServiceCentersEditComponent,
        canActivate: [ServiceCentersDetailGuard],
        resolve: {
          serviceCenter: ServiceCentersDetailsResolver
        } 
      },
      { path: 'services-centers-create', 
        component: ServiceCentersEditComponent
      },
      { path: 'distributors', 
        component: DistributorsTables 
      },
      { path: 'distributors/:id', 
        component: DistributorsDetailComponent,
        canActivate: [DistributorsDetailGuard],
        resolve: {
          distributor: DistributorsDetailsResolver
        } 
      },
      { path: 'distributors-edit/:id', 
        component: DistributorsEditComponent,
        canActivate: [DistributorsDetailGuard],
        resolve: {
          distributor: DistributorsDetailsResolver
        } 
      },
      { path: 'distributors-create', 
        component: DistributorsEditComponent
      },
      { path: 'research', 
        component: ResearchTables 
      },
      { path: 'research/:id', 
        component: ResearchDetailComponent,
        canActivate: [ResearchDetailGuard],
        resolve: {
          research: ResearchDetailsResolver
        } 
      },
      { path: 'actions', 
        component: ActionsTables 
      },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
