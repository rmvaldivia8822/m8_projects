export class Lead {
  id: number;
  name: string;
  platform: string;
  scoped_id: string;  
  optin_option: number;
  confirmed: any;
  confirmed_info: number;
  tools: number;
  optin_promotions: number;
  optin_product_research: number;
  country: string;
  used_maintenance: number;
  profile: any;
}
