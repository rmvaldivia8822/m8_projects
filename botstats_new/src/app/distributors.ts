export class Distributors {
  id: number;
  name: string;
  latitude: number;
  longitude: number;
  address: string;
  city: string;
  state: string;
  country: string;
  zipcode: string;
  province: string;
  phone: string;
  google_maps_url: string;
  website_url: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}