import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import * as Chart from 'chart.js';
import * as moment from 'moment';

import { BotstatsService } from './botstats.service';

@Component({
  selector: 'app-stats-graphic',
  templateUrl: './stats-graphic.component.html',
  styleUrls: ['./stats-graphic.component.scss']
})
export class StatsGraphicComponent implements OnInit {
  @Output() activeChart = new EventEmitter<string>();
  statsData: any;
  statChart;
  ctx: HTMLCanvasElement;
  chartLabels: any[] = [];
  leadStats: any[] = [];
  toolStats: any[] = [];
  referralStats: any[] = [];
  actionStats: any[] = [];
  chartData: any;
  chartFontFamily: string = '-apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif';
  chartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: 'bottom',
      labels: {
        fontFamily: this.chartFontFamily,
        boxWidth: 20
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontFamily: this.chartFontFamily
        }
      }],
      xAxes: [{
        ticks: {
          fontFamily: this.chartFontFamily
        }
      }]
    }
  };
  activeGraph: string;
  activeClass = {};

  constructor(private botstatsService: BotstatsService) {}

  ngOnInit(): void {
    this.botstatsService.getTotalStatsByDay()
      .then(dynamicStats => {
        this.statsData = dynamicStats;
        this.activeGraph = 'byDay';
        this.buildGraph(dynamicStats);
        this.activeChart.emit(this.activeGraph);
      });
  }

  buildTotalsByDay(): void {
    this.statChart.update();
    this.botstatsService.getTotalStatsByDay()
      .then(dynamicStats => {
        this.statsData = dynamicStats;
        this.activeGraph = 'byDay';
        this.buildGraph(dynamicStats);
        this.activeChart.emit(this.activeGraph);
      });
  }

  buildOnlyThisWeek(): void {
    this.statChart.update();
    this.botstatsService.getWeeklyStatsByDay()
      .then(dynamicStats => {
        this.statsData = dynamicStats;
        this.activeGraph = 'thisWeek';
        this.buildGraph(dynamicStats);
        this.activeChart.emit(this.activeGraph);
      });
  }

  buildGraph(dynamicStats: any): void {
    this.chartLabels = [];
    this.leadStats = [];
    this.toolStats = [];
    this.referralStats = [];
    this.actionStats = [];


    for (const value of dynamicStats.dates) {
      this.chartLabels.push(moment(value).fromNow());
    }

    for (const prop in dynamicStats.data.leads.values) {
      this.leadStats.push(dynamicStats.data.leads.values[prop]);
      this.toolStats.push(dynamicStats.data.tools.values[prop]);
      this.referralStats.push(dynamicStats.data.referrals.values[prop]);
      this.actionStats.push(dynamicStats.data.actions.values[prop]);
    }

    this.chartData = {
      labels: this.chartLabels,
      datasets: [
        {
          label: 'New Leads',
          data: this.leadStats,
          borderWidth: 4,
          lineTension: 0.2,
          fill: false,
          backgroundColor: 'rgba(91,192,222,1)',
          borderColor: 'rgba(91,192,222,1)',
        },
        {
          label: 'Registered Tools',
          data: this.toolStats,
          borderWidth: 3,
          lineTension: 0.2,
          fill: false,
          backgroundColor: 'rgba(240,173,78,1)',
          borderColor: 'rgba(240,173,78,1)',
        },
        {
          label: 'Referrals',
          data: this.referralStats,
          borderWidth: 2,
          lineTension: 0.2,
          fill: false,
          backgroundColor: 'rgba(92,184,92,1)',
          borderColor: 'rgba(92,184,92,1)',
        },
        {
          label: 'Actions',
          data: this.actionStats,
          borderWidth: 1,
          lineTension: 0.2,
          fill: false,
          backgroundColor: 'rgba(217,83,79,1)',
          borderColor: 'rgba(217,83,79,1)',
        },
      ]
    };

    this.ctx = <HTMLCanvasElement> document.getElementById('statGraph');
    this.statChart = new Chart(this.ctx, {
      type: 'line',
      data: this.chartData,
      options: this.chartOptions
    });
  }

}
