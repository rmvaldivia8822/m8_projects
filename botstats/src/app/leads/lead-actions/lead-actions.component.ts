import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Action } from './actions';

@Component({
  moduleId: module.id,
  selector: 'app-lead-actions',
  templateUrl: './lead-actions.component.html',
  styleUrls: ['./lead-actions.component.scss']
})
export class LeadActionsComponent implements OnInit {
  actions: Action[] = [];
  cantActions: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.data
      .subscribe(data => {
        this.actions = data['actions'];
        if (!this.actions){
          this.router.navigate(['/leads']);
        }
        else
          this.cantActions = this.actions.length;
      });
  }

}