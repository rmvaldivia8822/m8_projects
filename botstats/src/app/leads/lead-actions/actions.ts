export class Action {
    id: number;
    type: string;
    name: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
    description: string;
    scoped_id: string;
    platform: string;
    metadata: string;
    ip: string;
    dialog: string;
    leads_id: number; 
}