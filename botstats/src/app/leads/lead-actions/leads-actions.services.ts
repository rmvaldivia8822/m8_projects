import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Action } from './actions';

@Injectable()
export class LeadsActionsService {

  private api_url = `${environment.wepa_url}api/leads`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getLeadActions(lead_id: number): Observable<Action[]> {
    return this.http
            .get(`${this.api_url}/${lead_id}/actions`, {headers: this.headers})
            .map((response: Response) => <Action[]> response.json().actions )
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
  }

}
