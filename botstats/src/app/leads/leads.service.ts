import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Lead } from './lead';

@Injectable()
export class LeadsService {

  private api_url = `${environment.wepa_url}api/leads`;
  private headers = new Headers({
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': `Bearer ${environment.wepa_access_token}`,
    'Content-Type': 'application/json'
  });

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }

  getLeads(page?: number): Observable<Lead[]> {
    let api_url = this.api_url;
    if (page) {
      api_url = `${api_url}?page=${page}`;
    }

    return this.http
            .get(api_url, {headers: this.headers})
            .map((response: Response) => <Lead[]> response.json().leads )
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
  }
  

  getLead(lead_id: number): Observable<Lead> {
    return this.http
            .get(`${this.api_url}/${lead_id}`, {headers: this.headers})
            .map((response: Response) => <Lead> response.json().lead )
            .do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    
  } 

}
