import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 

import { LeadRoutingModule } from './leads-routing.module'

import { MomentModule } from 'angular2-moment';
import { AgmCoreModule } from 'angular2-google-maps/core';

import { LeadsResolver, LeadDetailsResolver, LeadToolsResolver, LeadActionsResolver } from '../resolvers.service';

import { LeadsService } from './leads.service';
import { LeadsToolsService } from './lead-tools/leads-tools.services';
import { LeadsActionsService } from './lead-actions/leads-actions.services';

import { LeadsComponent } from './leads.component';
import { LeadsListComponent } from './leads-list/leads-list.component';
import { LeadDetailsComponent } from './lead-details/lead-details.component';
import { LeadToolsComponent } from './lead-tools/lead-tools.component';
import { LeadActionsComponent } from './lead-actions/lead-actions.component';
import { ToolsComponent } from './leads-shared/tools.component';
import { ActionsComponent } from './leads-shared/actions.component';

import { LeadDetailGuard } from './lead-details/lead-details-guard.service';
import { LeadActionsGuard } from './lead-actions/lead-actions-guard.service';
import { LeadToolsGuard } from './lead-tools/lead-tools-guard.service';

@NgModule({
  declarations: [
    LeadsComponent,
    LeadsListComponent,
    LeadDetailsComponent,
    LeadToolsComponent,
    LeadActionsComponent,
    ToolsComponent,
    ActionsComponent
  ],
  imports: [
    CommonModule,
    LeadRoutingModule,    
    MomentModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCnbOmmZ-XrknNvw5fWbBexlN5tUcMNFS8'
    }),
  ],
  providers: [
    LeadsService,
    LeadsResolver,
    LeadDetailsResolver,
    LeadsToolsService,
    LeadToolsResolver,
    LeadActionsResolver,
    LeadsActionsService,
    LeadDetailGuard,
    LeadActionsGuard,
    LeadToolsGuard
  ]
})
export class LeadModule {}
