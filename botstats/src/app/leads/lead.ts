export class Lead {
  id: number;
  name: string;
  platform: string;
  scoped_id: string;  
}
