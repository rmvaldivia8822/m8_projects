import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Lead } from '../lead';

@Component({
  moduleId: module.id,
  selector: 'app-lead-details',
  templateUrl: './lead-details.component.html',
  styleUrls: ['./lead-details.component.scss']
})
export class LeadDetailsComponent implements OnInit {
  lead: Lead;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.data
      .subscribe(data => {
        this.lead = data['user'];
        if (!this.lead){
          this.router.navigate(['/leads']);
        }
      });
  }

}
