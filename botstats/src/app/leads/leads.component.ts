import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Lead } from './lead';
import { LeadsService } from './leads.service';

@Component({
  moduleId: module.id,
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})
export class LeadsComponent implements OnInit {
  leadsServiceData;
  leadsList: Lead[] = [];
  currentPage: number;
  lastPage: number;

  constructor(
    private route: ActivatedRoute,
    private leadsService: LeadsService
  ) { }

  ngOnInit(): void {
    this.route.data
      .subscribe(data => {
        this.leadsServiceData = data['leads'];
        this.leadsList = this.leadsServiceData.data;
        this.currentPage = this.leadsServiceData.current_page;
        this.lastPage = this.leadsServiceData.last_page;
      });
  }

  buildPage(page: number): void {
    this.leadsService.getLeads(page)
      .map(data => {
        this.leadsServiceData = data;
        this.leadsList = this.leadsServiceData.data;
        this.currentPage = this.leadsServiceData.current_page;
        this.lastPage = this.leadsServiceData.last_page;
      });
  }

  createPagesRange(pages): number[] {
    const numbers: number[] = [];
    for (let i = 1; i <= pages; i++) {
      numbers.push(i);
    }
    return numbers;
  }

}
