import { Component, OnInit, Input } from '@angular/core';

import { Tool } from '../lead-tools/tool';

import { LeadsToolsService } from '../lead-tools/leads-tools.services';

@Component({
    selector: 'ai-tools-count',
    templateUrl: './tools.component.html'
})
export class ToolsComponent implements OnInit {
    @Input() lead_id: number = 0;
    tools: Tool[] = [];
    errorMessage: string;

    constructor(private leadsToolsService: LeadsToolsService){

    }

    ngOnInit(): void {
        this.leadsToolsService.getLeadTools(this.lead_id )
                .subscribe(tools => this.tools = tools,
                           error => this.errorMessage = <any>error);
    }
}