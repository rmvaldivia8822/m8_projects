import { Component, OnInit, Input } from '@angular/core';

import { Action } from '../lead-actions/actions';

import { LeadsActionsService } from '../lead-actions/leads-actions.services';

@Component({
    selector: 'ai-actions-count',
    templateUrl: './actions.component.html'
})
export class ActionsComponent implements OnInit {
    @Input() lead_id: number = 0;
    actions: Action[] = [];
    errorMessage: string;

    constructor(private leadsActionsService: LeadsActionsService){
    }

    ngOnInit(): void {
        this.leadsActionsService.getLeadActions(this.lead_id )
                .subscribe(actions => this.actions = actions,
                           error => this.errorMessage = <any>error);
    }
}