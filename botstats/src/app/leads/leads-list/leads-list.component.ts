import { Component, OnInit, Input } from '@angular/core';

import { Lead } from '../lead';


@Component({
  moduleId: module.id,
  selector: 'app-leads-list',
  templateUrl: './leads-list.component.html',
  styleUrls: ['./leads-list.component.scss']
})
export class LeadsListComponent implements OnInit {
  @Input() leads: Lead[] = [];

  constructor() { }

  ngOnInit()  {    
  }
    
  

}
