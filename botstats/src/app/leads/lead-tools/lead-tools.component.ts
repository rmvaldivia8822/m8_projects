import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Tool } from './tool';

@Component({
  moduleId: module.id,
  selector: 'app-lead-tools',
  templateUrl: './lead-tools.component.html',
  styleUrls: ['./lead-tools.component.scss']
})
export class LeadToolsComponent implements OnInit {
  tools: Tool[] = [];
  cantTools: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.data
      .subscribe(data => {
        this.tools = data['tools'];
        if (!this.tools){
          this.router.navigate(['/leads']);
        } else
          this.cantTools = this.tools.length;
      });
  }

}