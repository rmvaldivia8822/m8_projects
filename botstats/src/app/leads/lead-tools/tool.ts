export class Tool {
  id: number;
  leads_id: number;
  platform: string;
  scoped_id: string;
  registration_status: string;
  serial_number: string;
  manufacture_date: string;
  buy_date: string;
  buy_location: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  recognized_manufacture_date: string;
  recognized_buy_date: string;
}
