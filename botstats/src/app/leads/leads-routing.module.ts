import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LeadsResolver, LeadDetailsResolver, LeadToolsResolver, LeadActionsResolver } from '../resolvers.service';

import { LeadsComponent } from './leads.component';
import { LeadDetailsComponent } from './lead-details/lead-details.component';
import { LeadToolsComponent } from './lead-tools/lead-tools.component';
import { LeadActionsComponent } from './lead-actions/lead-actions.component';

import { LeadDetailGuard } from './lead-details/lead-details-guard.service';
import { LeadActionsGuard } from './lead-actions/lead-actions-guard.service';
import { LeadToolsGuard } from './lead-tools/lead-tools-guard.service';

export const appLeadRoutes: Routes = [
  {
    path: 'leads',
    component: LeadsComponent,
    resolve: {
      leads: LeadsResolver
    }
  },  
  {
    path: 'leads/:id',
    component: LeadDetailsComponent,
    canActivate: [LeadDetailGuard],
    resolve: {
      user: LeadDetailsResolver
    }
  },
  {
    path: 'leads/tools/:id',
    component: LeadToolsComponent,
    canActivate: [LeadToolsGuard],
    resolve: {
      tools: LeadToolsResolver
    }
  },
  {
    path: 'leads/actions/:id',
    component: LeadActionsComponent,
    canActivate: [LeadActionsGuard],
    resolve: {
      actions: LeadActionsResolver
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(appLeadRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LeadRoutingModule {}
