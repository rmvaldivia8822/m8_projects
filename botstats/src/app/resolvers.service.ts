import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { TotalStat } from './stat-box/totalstat';
import { Lead } from './leads/lead';
import { Tool } from './leads/lead-tools/tool';
import { Action } from './leads/lead-actions/actions';
import { BotstatsService } from './stats-graphic/botstats.service';
import { LeadsService } from './leads/leads.service';
import { LeadsToolsService } from './leads/lead-tools/leads-tools.services';
import { LeadsActionsService } from './leads/lead-actions/leads-actions.services';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class OverviewResolver implements Resolve<any> {

  constructor(
    private bs: BotstatsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<TotalStat[]> {
    return this.bs.getTotalStats()
      .then(stats => {
        return stats;
      });
  }
}

@Injectable()
export class LeadsResolver implements Resolve<any> {

  constructor(
    private ls: LeadsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    return this.ls.getLeads(route.queryParams['page'])
      .map(data => {
        return data;
      });
  }
}

@Injectable()
export class LeadDetailsResolver implements Resolve<any> {

  constructor(
    private ls: LeadsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Lead> {
    return this.ls.getLead(route.params['id'])
      .map(lead => {
        if (lead)
          return lead;
        else {
          this.router.navigate(['/overview']);
        } 
      });
  }
}

@Injectable()
export class LeadToolsResolver implements Resolve<any> {

  constructor(
    private lts: LeadsToolsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Tool[]> {
    return this.lts.getLeadTools(route.params['id'])
      .map(tools => {
        if (tools)
          return tools;
        else {
          this.router.navigate(['/overview']);
        } 
      });
  }
}


@Injectable()
export class LeadActionsResolver implements Resolve<any> {

  constructor(
    private las: LeadsActionsService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Action[]> {
    return this.las.getLeadActions(route.params['id'])
      .map(actions => {
        if (actions)
          return actions;
        else {
          this.router.navigate(['/overview']);
        } 
      });
  }
}
