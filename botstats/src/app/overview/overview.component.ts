import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { TotalStat } from '../stat-box/totalstat';
import { BotstatsService } from '../stats-graphic/botstats.service';

@Component({
  moduleId: module.id,
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  actions: TotalStat;
  leads: TotalStat;
  tools: TotalStat;
  referrals: TotalStat;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private botstatsService: BotstatsService
  ) {}

  ngOnInit(): void {
    this.route.data
      .subscribe(data => {
        this.actions = data['totals'].actions;
        this.leads = data['totals'].leads;
        this.tools = data['totals'].tools;
        this.referrals = data['totals'].referrals;
      });
  }

  onChartChange(showing: string): void {
    if (showing === 'thisWeek') {
      this.botstatsService.getWeeklyStats()
        .then(data => {
          this.actions = data['actions'];
          this.leads = data.leads;
          this.tools = data.tools;
          this.referrals = data.referrals;
        });
    } else {
      this.ngOnInit();
    }
  }

}
