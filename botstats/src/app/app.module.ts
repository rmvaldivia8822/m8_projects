import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MomentModule } from 'angular2-moment';
import { AgmCoreModule } from 'angular2-google-maps/core';

import { OverviewResolver } from './resolvers.service';

import { AppRoutingModule } from './app-routing.module';
import { LeadModule } from './leads/leads.module';
import { BotstatsService } from './stats-graphic/botstats.service';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { StatBoxComponent } from './stat-box/stat-box.component';
import { StatsGraphicComponent } from './stats-graphic/stats-graphic.component';
import { OverviewComponent } from './overview/overview.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    StatBoxComponent,
    StatsGraphicComponent,
    OverviewComponent,
    PageNotFoundComponent,
    OverviewComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    LeadModule    
  ],
  providers: [
    BotstatsService,
    OverviewResolver
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
