import { Component, Input } from '@angular/core';

import { TotalStat } from './totalstat';

@Component({
  moduleId: module.id,
  selector: 'app-stat-box',
  templateUrl: './stat-box.component.html',
  styleUrls: ['./stat-box.component.scss'],
})
export class StatBoxComponent {
  @Input() stat: TotalStat;
}
