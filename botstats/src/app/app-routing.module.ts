import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OverviewResolver, LeadsResolver, LeadDetailsResolver, LeadToolsResolver, LeadActionsResolver } from './resolvers.service';

import { OverviewComponent } from './overview/overview.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { LeadsComponent } from './leads/leads.component';
import { LeadDetailsComponent } from './leads/lead-details/lead-details.component';
import { LeadToolsComponent } from './leads/lead-tools/lead-tools.component';
import { LeadActionsComponent } from './leads/lead-actions/lead-actions.component';

import { LeadDetailGuard } from './leads/lead-details/lead-details-guard.service';
import { LeadActionsGuard } from './leads/lead-actions/lead-actions-guard.service';
import { LeadToolsGuard } from './leads/lead-tools/lead-tools-guard.service';

export const appRoutes: Routes = [
  {
    path: 'overview',
    component: OverviewComponent,
    resolve: {
      totals: OverviewResolver
    }
  },  
  {
    path: 'leads',
    component: LeadsComponent,
    resolve: {
      leads: LeadsResolver
    }
  },  
  {
    path: 'leads/:id',
    component: LeadDetailsComponent,
    canActivate: [LeadDetailGuard],
    resolve: {
      user: LeadDetailsResolver
    }
  },
  {
    path: 'leads/tools/:id',
    component: LeadToolsComponent,
    canActivate: [LeadToolsGuard],
    resolve: {
      tools: LeadToolsResolver
    }
  },
  {
    path: 'leads/actions/:id',
    component: LeadActionsComponent,
    canActivate: [LeadActionsGuard],
    resolve: {
      actions: LeadActionsResolver
    }
  },
  {
    path: '',
    redirectTo: '/overview',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
