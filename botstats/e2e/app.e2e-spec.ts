import { BotstatsPage } from './app.po';

describe('botstats App', function() {
  let page: BotstatsPage;

  beforeEach(() => {
    page = new BotstatsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
