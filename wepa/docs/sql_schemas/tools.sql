/*
 Navicat MySQL Data Transfer

 Source Server         : mysql dev @ google (oscar Feb 2017)
 Source Server Type    : MySQL
 Source Server Version : 50714
 Source Host           : 104.196.130.151
 Source Database       : wepa

 Target Server Type    : MySQL
 Target Server Version : 50714
 File Encoding         : utf-8

 Date: 03/06/2017 11:49:59 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `tools`
-- ----------------------------
DROP TABLE IF EXISTS `tools`;
CREATE TABLE `tools` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leads_id` int(10) unsigned NOT NULL,
  `platform` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `scoped_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registration_status` char(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INCOMPLETE',
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacture_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buy_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buy_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `recognized_manufacture_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recognized_buy_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
