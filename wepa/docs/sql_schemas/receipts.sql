/*
 Navicat MySQL Data Transfer

 Source Server         : mysql dev @ google (oscar Feb 2017)
 Source Server Type    : MySQL
 Source Server Version : 50714
 Source Host           : 104.196.130.151
 Source Database       : wepa

 Target Server Type    : MySQL
 Target Server Version : 50714
 File Encoding         : utf-8

 Date: 03/06/2017 11:49:41 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `receipts`
-- ----------------------------
DROP TABLE IF EXISTS `receipts`;
CREATE TABLE `receipts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leads_id` int(10) unsigned NOT NULL,
  `platform` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `scoped_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saved_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extension` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tools_id` int(10) unsigned NOT NULL,
  `ocr` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
