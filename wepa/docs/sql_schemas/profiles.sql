/*
 Navicat MySQL Data Transfer

 Source Server         : mysql dev @ google (oscar Feb 2017)
 Source Server Type    : MySQL
 Source Server Version : 50714
 Source Host           : 104.196.130.151
 Source Database       : wepa

 Target Server Type    : MySQL
 Target Server Version : 50714
 File Encoding         : utf-8

 Date: 03/06/2017 11:49:28 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `profiles`
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `leads_id` int(10) unsigned NOT NULL,
  `platform` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `scoped_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `research` json NOT NULL,
  `extended_profile` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
