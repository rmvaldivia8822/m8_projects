<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tool extends Model
{
    use SoftDeletes;

    protected $table = 'tools';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'leads_id',
        'platform',
        'scoped_id',
        'registration_status',
        'serial_number',
        'manufacture_date',
        'buy_date',
        'buy_location',
        'deleted_at',
        'recognized_manufacture_date',
        'recognized_buy_date'
    ];

    public function lead() {
        return $this->belongsTo('App\Lead', 'leads_id');
    }

    public function receipt() {
        return $this->hasMany('App\Receipt', 'tools_id');
    }
}
