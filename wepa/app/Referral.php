<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referral extends Model
{
    use SoftDeletes;

    protected $table = 'referrals';
    protected $dates = ['deleted_at'];
    protected $fillable = ['leads_id', 'platform', 'scoped_id', 'source', 'campaign', 'referrer', 'metadata', 'deleted_at', 'ip'];

    public function lead() {
        return $this->belongsTo('App\Lead', 'leads_id');
    }
}
