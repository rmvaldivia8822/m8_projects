<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
    use SoftDeletes;

    protected $table = 'actions';
    protected $dates = ['deleted_at'];
    protected $fillable = ['type', 'name', 'created_at', 'scoped_id', 'platform', 'updated_at', 'description', 'who', 'who_type', 'metadata', 'ip', 'dialog', 'leads_id'];
}
