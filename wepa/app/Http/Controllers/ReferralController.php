<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Referral;

class ReferralController extends Controller
{

    public function index()
    {
        try {
            $referrals = Referral::paginate(25);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'referrals' => $referrals->toArray(),
        ), 200);
    }

    public function create(Request $request)
    {

            $referral = new Referral;
            $referral->leads_id = $request->leads_id;
            $referral->platform = $request->platform;
            $referral->scoped_id = $request->scoped_id;

            if($request->has('payload')){
                $referral->metadata = base64_decode($request->payload);
                $referral_params = $referral->metadata;
                $referral_params_array = array();
                parse_str($referral_params, $referral_params_array);

                if(array_key_exists('utm_campaign', $referral_params_array)) {
                    $referral->campaign = $referral_params_array['utm_campaign'];
                }

                if(array_key_exists('d1', $referral_params_array)) {
                    $referral->campaign = $referral_params_array['d1'];
                }

            } else {
                return response(array(
                    'error' => [
                        'errorInfo' => [
                            'Field \'payload\' is required'
                        ]
                    ]
                ));
            }

        try {
            $referral->save();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Referral created successfully',
            'referral' => $referral
        ), 200);
    }

    public function show($id)
    {
        try {
            $referral = Referral::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'referral' => $referral
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $referral = Referral::find($id);
            $referral->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Referral updated successfully',
            'referral' => $referral
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $referral = Referral::find($id);
            $referral->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Referral deleted successfully',
            'referral' => $referral
        ), 200);
    }
}
