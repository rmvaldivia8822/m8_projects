<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Lead;
use App\Profile;

class ProfileController extends Controller
{

    public function index(Request $request)
    {
        $limit = 25;
        if ( is_numeric($request->limit) )
            $limit = $request->limit;
        try {
            $profiles = Profile::orderBy('profiles.id', 'desc')
            ->leftJoin('leads', function($join){
                $join->on('profiles.leads_id', '=', 'leads.id');
            })
            ->where(function($query) use ($request){
                if ( $request->name )
                    $query->Where('leads.name', 'LIKE', '%'.$request->name.'%');
                if ( $request->research )
                    $query->Where('profiles.research', 'LIKE', '%'.$request->research.'%');
            })
            ->paginate($limit);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'profiles' => $profiles,
        ), 200);
    }

    public function create(Request $request)
    {
        try {
            $profile = Profile::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Profile created successfully',
            'profile' => $profile
        ), 200);
    }

    public function show($id)
    {
        try {
            $profile = Profile::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'profile' => $profile
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $profile = Profile::find($id);
            $profile->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Profile updated successfully',
            'profile' => $profile
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $profile = Profile::find($id);
            $profile->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Profile deleted successfully',
            'profile' => $profile
        ), 200);
    }

    public function exportPdf(Request $request)
    {
         try {
            $profiles = Profile::orderBy('profiles.id', 'desc')
            ->leftJoin('leads', function($join){
                $join->on('profiles.leads_id', '=', 'leads.id');
            })
            ->where(function($query) use ($request){
                if ( $request->leads_id )
                    $query->Where('leads.name', 'LIKE', '%'.$request->leads_id.'%');
                if ( $request->research )
                    $query->Where('profiles.research', 'LIKE', '%'.$request->research.'%');
            })
            ->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'profiles' => $profiles,
        ), 200);
    }

}
