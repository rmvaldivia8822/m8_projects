<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    public function getUsersBy($type, $id) {
        try {
            $users = User::where($type, $id)->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'query' => 'WHERE `'.$type.'` = `'.$id.'`',
            'count' => $users->count(),
            'users' => $users
        ));
    }
    
}
