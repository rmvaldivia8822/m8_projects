<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Distributor;

class DistributorController extends Controller
{
    public function index(Request $request)
    {
        $limit = 25;
        if ( is_numeric($request->limit) )
            $limit = $request->limit;   
        try {
            $distributors = Distributor::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->id )
                    $query->Where('id', 'LIKE', '%'.$request->id.'%' );
                if ( $request->name )
                    $query->Where('name', 'LIKE', '%'.$request->name.'%');
                if ( $request->country )
                    $query->Where('country', 'LIKE', '%'.$request->country.'%');
                if ( $request->phone )
                    $query->Where('phone', 'LIKE', '%'.$request->phone.'%');
            })
            ->paginate($limit);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'distributors' => $distributors,
        ), 200);
    }

    public function create(Request $request)
    {
        try {
            $distributor = Distributor::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex->getTrace()
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Distributor created succesfully',
            'distributor' => $distributor->toArray()
        ), 200);
    }

    public function show($id)
    {
        try {
            $distributor = Distributor::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'distributor' => $distributor
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $distributor = Distributor::find($id);
            $distributor->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Distributor updated successfully',
            'distributor' => $distributor
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $distributor = Distributor::find($id);
            $distributor->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Distributor deleted successfully',
            'distributor' => $distributor
        ), 200);
    }


    public function findNear(Request $request)
    {
        if($request->has('latitude') && $request->has('longitude'))
        {
            $lat = $request->latitude;
            $long = $request->longitude;
            $radius = 50;

            $near_distributors = DB::table('distributors')
                ->select(DB::raw('*,
                    ( 6371 * acos( cos( radians(?) ) *
                    cos( radians( latitude ) )
                    * cos( radians( longitude ) - radians(?)
                    ) + sin( radians(?) ) *
                    sin( radians( latitude ) ) )
                    ) AS distance'))
                ->having("distance", "<", "?")
                ->orderBy("distance")
                ->limit(10)
                ->setBindings([$lat, $long, $lat,  $radius])
                ->get();

            return response(array(
                'error' => false,
                'latitude' => $lat,
                'longitude' => $long,
                'radius' => $radius,
                'distributors' => $near_distributors
            ), 200);
        } else {
            return response(array(
                'error' => 'Fields \'latitude\' and \'longitude\' are required!'
            ));
        }
    }

    public function exportPdf(Request $request)
    {
        try {
            $distributors = Distributor::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->id )
                    $query->Where('id', 'LIKE', '%'.$request->id.'%' );
                if ( $request->name )
                    $query->Where('name', 'LIKE', '%'.$request->name.'%');
                if ( $request->country )
                    $query->Where('country', 'LIKE', '%'.$request->country.'%');
                if ( $request->phone )
                    $query->Where('phone', 'LIKE', '%'.$request->phone.'%');
            })->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'distributors' => $distributors,
        ), 200);
    }
}
