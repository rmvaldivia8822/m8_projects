<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Lead;
use App\Receipt;
use App\Tool;
use App\Referral;
use App\Profile;
use App\Action;
use App\Distributor;
use App\ServiceCenter;
use Carbon\Carbon;
use DateTime;
use DateInterval;
use DatePeriod;

class StatController extends Controller
{
    private $metrics = [
        'leads' => 'App\Lead',
        'referrals' => 'App\Referral',
        'tools' => 'App\Tool',
        'profiles' => 'App\Profile',
        'receipts' => 'App\Receipt',
        'actions' => 'App\Action'
    ];

    private $tables = [
        'actions',
        'distributors',
        'leads',
        'profiles',
        'receipts',
        'referrals',
        'servicecenters',
        'tools'
    ];

    public function getTotals() {
        $stats = [];

        foreach($this->metrics AS $key => $value)
        {
            $stats[$key] = array(
                'metric' => $key,
                'total' => $value::count(),
            );
        }

        return response(array(
            'generated_on' => date('l jS \of F Y h:i:s A'),
            'data' => $stats
        ), 200);
    }

    public function getThisWeekTotalsByDay() {

        $stats = [];
        $dates = [];

        $fromDate = Carbon::now()->subDay()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->subDay()->toDateString();
        $tillTomorrowDate = Carbon::now()->subDay()->tomorrow()->toDateString();

        $begin = new DateTime($fromDate);
        $end = new DateTime($tillTomorrowDate);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        foreach($period as $dt)
            $dates[] = $dt->format( "Y-m-d" );

        foreach($this->metrics AS $metric => $class)
        {
            $aggregation = $class::selectRaw('date(created_at) as date, COUNT(*) as count')
                ->whereBetween( DB::raw('date(created_at)'), [$fromDate, $tillTomorrowDate] )
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->pluck('count', 'date');

            $stats[$metric] = array(
                'metric' => $metric,
                'dates' => $aggregation
            );

            foreach($dates AS $date){
                if(!isset($stats[$metric]['dates'][$date])) {
                    $stats[$metric]['values'][$date] = 0;
                } else {
                    $stats[$metric]['values'][$date] = $stats[$metric]['dates'][$date];
                }
            }

        }

        return response(array(
            'generated_on' => date('l jS \of F Y h:i:s A'),
            'query' => 'get this week totals by day',
            'from' => $fromDate,
            'to' => $tillDate,
            'dates' => $dates,
            'data' => $stats,
        ), 200);
    }

    public function getThisWeek() {

        $stats = [];
        $dates = [];

        $fromDate = Carbon::now()->subDay()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->subDay()->toDateString();
        $tillTomorrowDate = Carbon::now()->subDay()->tomorrow()->toDateString();

        $begin = new DateTime($fromDate);
        $end = new DateTime($tillTomorrowDate);
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        foreach($period as $dt)
            $dates[] = $dt->format( "Y-m-d" );

        foreach($this->metrics AS $metric => $class)
        {
            $aggregation = $class::selectRaw('COUNT(*) as count')
                ->whereBetween( DB::raw('date(created_at)'), [$fromDate, $tillTomorrowDate] )
                ->pluck('count');

            $stats[$metric] = array(
                'metric' => $metric,
                'total' => $aggregation[0]
            );

        }

        return response(array(
            'generated_on' => date('l jS \of F Y h:i:s A'),
            'query' => 'get this week totals',
            'from' => $fromDate,
            'to' => $tillDate,
            'dates' => $dates,
            'data' => $stats,
        ), 200);
    }

    public function getTotalsByDay() {
        $stats = [];
        $dates = [];
        $finalDates = [];

        foreach($this->metrics AS $metric => $class)
        {
            $aggregation = $class::selectRaw('date(created_at) as date, COUNT(*) as count')
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->pluck('count', 'date');

            $stats[$metric] = array(
                'metric' => $metric,
                'values' => $aggregation
            );

            foreach($aggregation AS $key => $val)
            {
                $dates[] = strtotime($key);
            }
        }

        $clean_dates = array_unique($dates);
        sort($clean_dates);

        foreach($clean_dates AS $date) {
            $finalDates[] = date('Y-m-d', $date);
        }

        foreach($this->metrics AS $metric => $class)
        {
            foreach($finalDates as $date)
            {
                if(!isset($stats[$metric]['values'][$date])) {
                    $stats[$metric]['temp'][$date] = 0;
                } else {
                    $stats[$metric]['temp'][$date] = $stats[$metric]['values'][$date];
                }
            }

            $stats[$metric]['values'] = $stats[$metric]['temp'];
            unset($stats[$metric]['temp']);
        }




        return response(array(
            'generated_on' => date('l jS \of F Y h:i:s A'),
            'query' => 'get totals by day',
            'dates' => $finalDates,
            'data' => $stats,
        ), 200);

    }

}
