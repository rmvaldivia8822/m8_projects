<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Lead;
use App\Receipt;
use Intervention\Image\Facades\Image;

class ReceiptController extends Controller
{
    public function index()
    {
        try {
            $leads = Receipt::paginate(25);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'receipts' => $leads->toArray(),
        ), 200);
    }


    public function create(Request $request)
    {

        try {
            $receipt = Receipt::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex,
                'error_step' => 'receipt',
                'request' => $request->all()
            ));
        }

        if (!isset($receipt['file_url']) || $receipt['file_url'] === '') {
            return response(array(
                'error' => false,
                'message' => 'Empty receipt saved successfully',
                'request' => $request->all(),
                'saved_to' =>  '',
                'file_name' => '',
                'extension' => '',
                'receipt' => $receipt
            ), 200);
        }

        try {
            $filename = 'LID_'.$request->leads_id.'__RID_'.$receipt->id.'.jpg';
            $path = storage_path('receipts/'.$filename);
            $image = Image::make($request->input('file_url'))->save($path);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex,
                'error_step' => 'file_url',
                'request' => $request->all()
            ));
        }

        try {
            $receipt->file_name = $filename;
            $receipt->saved_to = $path;
            $receipt->file_title = 'Receipt uploaded on '.date(DATE_RFC2822).' by user ID '.$request->leads_id;
            $receipt->save();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex,
                'error_step' => 'update',
                'request' => $request->all()
            ));
        }


        return response(array(
            'error' => false,
            'message' => 'Receipt saved successfully',
            'request' => $request->all(),
            'saved_to' => $path,
            'file_name' => $filename,
            'extension' => '',
            'receipt' => $receipt
        ), 200);

    }


    public function show($id)
    {
        try {
            $receipt = Receipt::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'receipt' => $receipt
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $receipt = Receipt::find($id);
            $receipt->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Receipt deleted successfully',
            'receipt' => $receipt
        ), 200);
    }
}



