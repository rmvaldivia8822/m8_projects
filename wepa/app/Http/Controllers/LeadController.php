<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use App\Receipt;
use App\Tool;
use App\Referral;
use App\Profile;

class LeadController extends Controller
{

    public function index(Request $request)
    {
        $limit = 25;
        if ( is_numeric($request->limit) )
            $limit = $request->limit;   
        try {
            $leads = Lead::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->name )
                    $query->Where('name', 'LIKE', '%'.$request->name.'%');
                if ( $request->country )
                    $query->Where('country', 'LIKE', '%'.$request->country.'%');
            })
            ->paginate($limit);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads' => $leads,
        ), 200);
    }

    public function create(Request $request)
    {
        try {
            $lead = Lead::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Lead created successfully',
            'lead' => $lead
        ), 200);
    }

    public function show($id)
    {
        try {
            $lead = Lead::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'lead' => $lead
        ), 200);
    }

    public function getLeadBy($type, $id) {
        try {
            $lead = Lead::where($type, $id)->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'query' => 'WHERE `'.$type.'` = `'.$id.'`',
            'count' => $lead->count(),
            'lead' => $lead
        ));
    }

    public function update(Request $request, $id)
    {
        try {
            $lead = Lead::find($id);
            $lead->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Lead updated successfully',
            'lead' => $lead
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $lead = Lead::find($id);
            $lead->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Lead deleted successfully',
            'lead' => $lead
        ), 200);
    }

    public function showReceipts($id)
    {
        try {
            $receipts = Lead::find($id)->receipts;
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads_id' => $id,
            'receipts' => $receipts
        ), 200);
    }

    public function showTools($id)
    {

        try {
            $tools = Lead::find($id)->tools;
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads_id' => $id,
            'tools' => $tools
        ), 200);
    }

    public function showReferrals($id)
    {
        try {
            $referrals = Lead::find($id)->referrals;
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads_id' => $id,
            'referrals' => $referrals
        ), 200);
    }

    public function showActions($id)
    {
        try {
            $actions = Lead::find($id)->actions;
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads_id' => $id,
            'actions' => $actions
        ), 200);
    }

    public function showProfiles($id)
    {
        try {
            $profiles = Lead::find($id)->profiles;
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads_id' => $id,
            'profiles' => $profiles
        ), 200);
    }


    public function confirmedInfo($id)
    {
        $confirmed_info = false;
        $message = 'User HAS NOT confirmed the basic profile information';
        try {
            $lead = Lead::find($id);
            $confirmed_info = $lead->confirmed_info;
            if($confirmed_info) { $message = 'User confirmed the basic profile information'; }
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'leads_id' => $id,
            'confirmed_info' => $confirmed_info,
            'message' => $message
        ));
    }

    public function exportPdf(Request $request)
    {
        try {
            $leads = Lead::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->name )
                    $query->Where('name', 'LIKE', '%'.$request->name.'%');
                if ( $request->country )
                    $query->Where('country', 'LIKE', '%'.$request->country.'%');
            })->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads' => $leads,
        ), 200);
    }

    public function search(Request $request)
    {
        try {
            $leads = Lead::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->search )
                    $query->Where('name', 'LIKE', '%'.$request->search.'%');
                if ( $request->search )
                    $query->Where('email', 'LIKE', '%'.$request->search.'%');
            })->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'leads' => $leads,
        ), 200);
    }
}
