<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\ServiceCenter;

class ServiceCenterController extends Controller
{
    public function index(Request $request)
    {
        $limit = 25;
        if ( is_numeric($request->limit) )
            $limit = $request->limit;   
        try {
            $servicecenters = ServiceCenter::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->id )
                    $query->Where('id', 'LIKE', '%'.$request->id.'%' );
                if ( $request->name )
                    $query->Where('name', 'LIKE', '%'.$request->name.'%');
                if ( $request->country )
                    $query->Where('country', 'LIKE', '%'.$request->country.'%');
                if ( $request->phone )
                    $query->Where('phone', 'LIKE', '%'.$request->phone.'%');
            })
            ->paginate($limit);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'servicecenters' => $servicecenters,
        ), 200);
    }

    public function create(Request $request)
    {
        try {
            $servicecenter = ServiceCenter::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex->getTrace()
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Service Center created succesfully',
            'servicecenter' => $servicecenter
        ), 200);
    }

    public function show($id)
    {
        try {
            $servicecenter = ServiceCenter::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'servicecenter' => $servicecenter
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $servicecenter = ServiceCenter::find($id);
            $servicecenter->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Service Center updated successfully',
            'servicecenter' => $servicecenter
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $servicecenter = ServiceCenter::find($id);
            $servicecenter->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'ServiceCenter deleted successfully',
            'servicecenter' => $servicecenter
        ), 200);
    }

    public function findNear(Request $request)
    {
        if($request->has('latitude') && $request->has('longitude'))
        {
            $lat = $request->latitude;
            $long = $request->longitude;
            $radius = 50;

            $near_servicecenters = DB::table('servicecenters')
                ->select(DB::raw('*,
                    ( 6371 * acos( cos( radians(?) ) *
                    cos( radians( latitude ) )
                    * cos( radians( longitude ) - radians(?)
                    ) + sin( radians(?) ) *
                    sin( radians( latitude ) ) )
                    ) AS distance'))
                ->having("distance", "<", "?")
                ->orderBy("distance")
                ->limit(10)
                ->setBindings([$lat, $long, $lat,  $radius])
                ->get();

            return response(array(
                'error' => false,
                'latitude' => $lat,
                'longitude' => $long,
                'radius' => $radius,
                'distributors' => $near_servicecenters
            ), 200);
        } else {
            return response(array(
                'error' => 'Fields \'latitude\' and \'longitude\' are required!'
            ));
        }
    }

    public function exportPdf(Request $request)
    {
        try {
            $servicecenters = ServiceCenter::orderBy('id', 'desc')
            ->where(function($query) use ($request){
                if ( $request->id )
                    $query->Where('id', 'LIKE', '%'.$request->id.'%' );
                if ( $request->name )
                    $query->Where('name', 'LIKE', '%'.$request->name.'%');
                if ( $request->country )
                    $query->Where('country', 'LIKE', '%'.$request->country.'%');
                if ( $request->phone )
                    $query->Where('phone', 'LIKE', '%'.$request->phone.'%');
            })->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'servicecenters' => $servicecenters,
        ), 200);
    }
}
