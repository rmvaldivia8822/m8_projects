<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Lead;
use App\Receipt;

class ToolController extends Controller
{

    public function index()
    {
        try {
            $tools = Tool::paginate(25);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'tools' => $tools->toArray(),
        ), 200);
    }

    public function create(Request $request)
    {
        try {
            $tool = Tool::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Tool registration created successfully',
            'tool' => $tool
        ), 200);
    }

    public function show($id)
    {
        try {
            $tool = Tool::find($id);
            $receipt = $tool->receipt()->get();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'receipt' => $receipt,
            'tool' => $tool
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $tool = Tool::find($id);
            $tool->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Tool registration updated successfully',
            'tool' => $tool
        ), 200);
    }

    public function destroy($id)
    {
        try {
            $tool = Tool::find($id);
            $tool->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Tool registration deleted successfully',
            'tool' => $tool
        ), 200);
    }
}
