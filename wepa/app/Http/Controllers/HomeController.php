<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use App\Receipt;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('home');
    }

    public function leads()
    {
        $leads = Lead::paginate(25);
        return view('leads', [
            'leads' => $leads
        ]);
    }

    public function authentication()
    {
        return view('authentication');
    }
}
