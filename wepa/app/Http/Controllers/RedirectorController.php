<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Action;

class RedirectorController extends Controller
{
    public function doRedirection(Request $request) {

        $parameters = $request->all();
        $query_string = $request->getQueryString();
        $encoded_query_string = base64_encode($query_string);
        $redirecting_to = env('FB_BOT_URL').'?ref='.$encoded_query_string;
        $redirection['parameters'] = $parameters;
        $redirection['ip'] = $request->ip();
        $redirection['url'] = $request->fullUrl();
        $redirection['query_string'] = $query_string;
        $redirection['redirected_to'] = $redirecting_to;
        $redirection['fingerprint'] = $request->fingerprint();
        $redirection['header'] = $request->header();

        $this->saveRedirectionAction($redirection);

        return redirect($redirecting_to);
    }

    public function saveRedirectionAction($redirection)
    {

        $parameter_count = count($redirection['parameters']);

        $action = new Action;
        $action->type = 'SYS';
        $action->name = 'REDIR';
        $action->description = 'Redirection from '.$redirection['header']['host'][0].' to '.$redirection['redirected_to'].' with '.$parameter_count.' parameters';
        $action->metadata = json_encode($redirection);
        $action->ip = $redirection['ip'];
        $action->save();
    }

}
