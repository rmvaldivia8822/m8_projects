<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCenter extends Model
{
    use SoftDeletes;

    protected $table = 'servicecenters';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'latitude', 'longitude', 'address', 'city', 'state', 'country', 'zipcode', 'province', 'phone', 'google_maps_url'];
}
