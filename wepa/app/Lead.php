<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;

    protected $table = 'leads';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'platform', 'scoped_id', 'created_at', 'updated_at', 'first_name', 'last_name', 'phone_number', 'gender', 'date_of_birth', 'email', 'country', 'city', 'address', 'fbid', 'optin_product_research', 'optin_promotions', 'latitude', 'longitude', 'zipcode', 'confirmed_info', 'used_maintenance'];

    public function receipts() {
        return $this->hasMany('App\Receipt', 'leads_id');
    }

    public function tools() {
        return $this->hasMany('App\Tool', 'leads_id');
    }

    public function referrals() {
        return $this->hasMany('App\Referral', 'leads_id');
    }

    public function profiles() {
        return $this->hasMany('App\Profile', 'leads_id');
    }

    public function actions() {
        return $this->hasMany('App\Action', 'leads_id');
    }
}
