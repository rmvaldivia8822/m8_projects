<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;

    protected $table = 'receipts';
    protected $dates = ['deleted_at'];
    protected $fillable = ['leads_id', 'tools_id', 'platform', 'scoped_id', 'file_url', 'saved_to', 'file_title', 'file_name', 'extension', 'deleted_at', 'ocr'];

    public function lead() {
        return $this->belongsTo('App\Lead', 'leads_id');
    }

    public function tool() {
        return $this->belongsTo('App\Tool');
    }
}
