<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    protected $table = 'profiles';
    protected $dates = ['deleted_at'];
    protected $fillable = ['leads_id', 'platform', 'scoped_id', 'research', 'extended_profile', 'deleted_at'];

    public function lead() {
        return $this->belongsTo('App\Lead', 'leads_id');
    }
}
