<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distributor extends Model
{
    use SoftDeletes;

    protected $table = 'distributors';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'latitude', 'longitude', 'address', 'city', 'state', 'country', 'zipcode', 'province', 'phone', 'google_maps_url'];
}

