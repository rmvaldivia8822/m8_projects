# WEPA

Chatbot "back-end" web API and dashboard with oAuth access token and user authentication.

## Installation

1. Clone the repo
2. Enable and configure the .env file on the root directory
3. Run the following commands
    1. `npm install` or `yarn install`, depending on your setup
    2. `composer install --prefer-dist`
    3. `php artisan migrate`
    4. `php artisan vendor:publish --tag=passport-components`
    5. `php artisan passport:install`
    6. `php artisan vendor:publish --provider="Intervention\Image\ImageServiceProviderLaravel5"`
    7. `php artisan config:publish intervention/image`
    8. `gulp --production`


## Web Dashboard

To be determined...


## API

- Create Access Tokens in the Web Dashboard after registering at https://wepa.m8loves.me/
- You must include an 'Authorization' header field with a value of 'Bearer {access token}' for authentication
- You must include an 'X-Requested-With' header field with a value of 'XMLHttpRequest' to receive proper JSON responses
- Take into consideration each Model data structure and requirements


### Leads (End-user)

| Verb | URI | Description |
|------|-----|-------------|
| GET | /leads | List all leads (paginated) |
| POST |    /leads | Create a lead |
| GET | /leads/search/{column}/{value}| Search for a lead based on a specific table column |
| GET |	/leads/{lead_id} | Show a lead information |
| GET |	/leads/{lead_id}/confirmed | Show if a lead has confirmed his basic profile information |
| GET | /leads/{lead_id}/tools | Show lead tool registrations |
| GET | /leads/{lead_id}/receipts | Show lead receipts |
| GET | /leads/{lead_id}/referrals | Show lead referrals |
| GET | /leads/{lead_id}/profiles | Show lead profiles |
| PUT/PATCH | /leads/{lead_id} | Update a lead |
| DELETE | /leads/{lead_id} |	Soft delete a lead |

### Tools

| Verb | URI | Description |
|------|-----|-------------|
| GET | /tools | List all tool registrations (paginated) |
| POST |    /tools |  Create a tool registration |
| GET | /tools/{tool_id} | Show tool registration information |
| DELETE | /tools/{tool_id} | Soft delete a tool registration |

### Receipts

| Verb | URI | Description |
|------|-----|-------------|
| GET | /receipts | List all receipts (paginated) |
| POST |    /receipts |  Create a receipt & stores the image in Filesystem |
| GET | /receipts/{receipt_id} | Show receipt information |
| DELETE | /receipts/{receipt_id} | Soft delete a receipt |

*Editing a receipt doesn't make much sense to me. Thoughts?*

### Actions (Log)

| Verb | URI | Description |
|------|-----|-------------|
| GET | /actions | List all actions (paginated) |
| GET | /actions/totals | Counted aggregation of all user actions |
| POST |    /actions |   Create an action |
| GET | /actions/{action_id} | Show action information |
| PUT/PATCH | /actions/{action_id} | Update an action |
| DELETE | /actions/{action_id} |   Soft delete an action |

### Referrals (Traffic Sources)

| Verb | URI | Description |
|------|-----|-------------|
| GET | /referrals | List all referrals (paginated) |
| POST |    /referrals |   Create a referral |
| GET | /referrals/{referral_id} | Show referral information |
| DELETE | /referrals/{referral_id} |   Soft delete a referral |

*Editing a referral doesn't make much sense to me. Thoughts?*

### Profile (Research & Extended Profiles)

| Verb | URI | Description |
|------|-----|-------------|
| GET | /profiles | List all profiles (paginated) |
| POST |    /profiles |   Create a profile |
| GET | /profiles/{profile_id} | Show profile information |
| DELETE | /profiles/{profile_id} |   Soft delete a profile |

### Service Centers

| Verb | URI | Description |
|------|-----|-------------|
| GET | /servicecenters | List all Service Centers (paginated) |
| POST | /servicecenters |   Create a Service Center |
| PUT/PATCH | /servicecenters/{servicecenter_id} | Update a Service Center |
| GET | /servicecenters/near | Returns all Service Centers near the provided lat/long. The fields 'latitude' and 'longitude' are required |
| GET | /servicecenters/{servicecenter_id} | Show Service Center information |
| DELETE | /servicecenters/{servicecenter_id} |   Soft delete a Service Center |

### Distributors

| Verb | URI | Description |
|------|-----|-------------|
| GET | /distributors | List all Distributors (paginated) |
| POST | /distributors |   Create a Distributor |
| PUT/PATCH | /distributors/{distributors_id} | Update a Distributor |
| GET | /distributors/near | Returns all Distributors near the provided lat/long. The fields 'latitude' and 'longitude' are required |
| GET | /distributors/{distributors_id} | Show Distributor information |
| DELETE | /distributors/{distributors_id} |   Soft delete a Distributor |

## SQL Schemas

Please find the schemas on the `docs/sql_schemas/` folder of this project!


## Bot Redirection for Tracking and Referral Parameters

Facebook Messenger links only allow us to send a single GET parameter to our bot, the `ref` parameters. For this reason, we are using a 'redirector' to send all the parameters we want, encoded as a base 64 string.

Instead of sending referred users (media, social, earned) to the m.me URL, we are using a `https://wepa.m8loves.me/redir` URL so that you can attach any amount of GET parameters.

For example, calling the following URL:
- https://wepa.m8loves.me/redir?d1=test&otracosa=otracosa&utm_source=atlas&utm_campaign=test&utm_medium=banner&utm_term=testingpower&utm_content=smiling_person

Will redirect users to the bot with the encoded parameters:
- https://m.me/t/381385082237780/?ref=ZDE9dGVzdCZvdHJhY29zYT1vdHJhY29zYSZ1dG1fY2FtcGFpZ249dGVzdCZ1dG1fY29udGVudD1zbWlsaW5nX3BlcnNvbiZ1dG1fbWVkaXVtPWJhbm5lciZ1dG1fc291cmNlPWF0bGFzJnV0bV90ZXJtPXRlc3Rpbmdwb3dlcg%3D%3D

Doing a redirection will trigger and save a log action with the user's IP!



