<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->char('type', 5); // USER, SYS, FB, others, ... 5 character limit
            $table->char('name', 10); // 10 character limit for action names
            $table->timestamps();
            $table->softDeletes();

            // Optional Fields
            $table->longText('description')->nullable(); // long description for action events
            $table->string('scoped_id')->nullable(); // User identifier, can be an internal ID, EMAIL, FBID, SFBID, OTHER
            $table->char('platform', 10)->nullable(); // 5 character limit for type of identifier
            $table->longText('metadata')->nullable(); // any additional meta-data
            $table->ipAddress('ip')->nullable();

            // TODO: add foreign key for origin bot
            // TODO: add foreign key for origin lead (user)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
