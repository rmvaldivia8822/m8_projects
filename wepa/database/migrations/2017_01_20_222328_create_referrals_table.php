<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referrals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leads_id')->unsigned();
            $table->char('platform', 10);
            $table->string('scoped_id');
            $table->string('metadata');

            // Optional
            $table->string('source')->nullable();
            $table->string('campaign')->nullable();
            $table->string('referrer')->nullable();
            $table->ipAddress('ip')->nullable();

            // Timestamps
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referrals');
    }
}
