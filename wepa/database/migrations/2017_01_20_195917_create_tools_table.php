<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leads_id')->unsigned();
            $table->char('platform', 10);
            $table->string('scoped_id');
            $table->char('registration_status', 10)->default('INCOMPLETE'); // Easy access registration Status: COMPLETE, INCOMPLETE

            // Optional
            $table->string('serial_number')->nullable(); // Tool's serial number
            $table->date('manufacture_date')->nullable(); // Date the tool was manufactured (?)
            $table->date('buy_date')->nullable(); // Date the tool was bought
            $table->string('buy_location')->nullable(); // Where the tool was bought?

            // Timestamps
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tools');
    }
}
