<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leads_id')->unsigned();
            $table->char('platform', 10);
            $table->string('scoped_id');
            $table->string('file_url');

            // Optional
            $table->string('saved_to')->nullable(); // if the file is saved, say to where it was saved
            $table->string('file_title')->nullable();
            $table->char('file_name')->nullable(); // good for future search capabilities of the dashboard
            $table->char('extension', 5)->nullable(); // the file's extention .txt, .jpg, etcetera

            // Timestamps
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
