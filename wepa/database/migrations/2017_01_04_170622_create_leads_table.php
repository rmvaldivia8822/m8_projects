<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('platform', 10);
            $table->string('scoped_id')->unique();

            // Optional Fields
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone_number')->nullable();
            $table->char('gender', 1)->nullable(); // M or F
            $table->date('date_of_birth')->nullable();
            $table->string('email')->nullable();
            $table->char('country', 3)->nullable();
            $table->string('city')->nullable();
            $table->longText('address')->nullable();
            $table->bigInteger('fbid')->unsigned()->unique()->nullable();
            $table->boolean('optin_product_research')->nullable();
            $table->boolean('optin_promotions')->nullable();

            // Timestamps
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
