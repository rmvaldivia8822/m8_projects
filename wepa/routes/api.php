<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/me', function (Request $request)    {
        return $request->user();
    });

    Route::get('/me/profile', function (Request $request) {
        return $request->user();
    });

/*
|--------------------------------------------------------------------------
| Leads Routes
|--------------------------------------------------------------------------
*/

    Route::post('/leads/create',                'LeadController@create');
    Route::post('/leads',                       'LeadController@create');
    Route::get('/leads/{id}',                   'LeadController@show');
    Route::get('/leads/search/{type}/{id}',     'LeadController@getLeadBy');
    Route::get('/leads/{id}/receipts',          'LeadController@showReceipts');
    Route::get('/leads/{id}/tools',             'LeadController@showTools');
    Route::get('/leads/{id}/referrals',         'LeadController@showReferrals');
    Route::get('/leads/{id}/profiles',          'LeadController@showProfiles');
    Route::get('/leads/{id}/actions',           'LeadController@showActions');
    Route::get('/leads/{id}/confirmed',         'LeadController@confirmedInfo');
    Route::post('/leads/export-pdf',            'LeadController@exportPdf');
    Route::post('/leads/search',                'LeadController@search');
    Route::resource('leads', 'LeadController', ['only' => [
        'index', 'destroy', 'update'
    ]]);

/*
|--------------------------------------------------------------------------
| Actions Routes
|--------------------------------------------------------------------------
*/
    Route::get('/actions/totals', 'ActionController@getTotals');
    Route::post('/actions/create', 'ActionController@create');
    Route::post('/actions', 'ActionController@create');
    Route::resource('actions', 'ActionController', ['only' => [
        'index', 'show', 'destroy', 'update'
    ]]);


/*
|--------------------------------------------------------------------------
| Tool registration Routes
|--------------------------------------------------------------------------
*/
    Route::post('/tools/create', 'ToolController@create');
    Route::post('/tools', 'ToolController@create');
    Route::resource('tools', 'ToolController', ['only' => [
        'index', 'show', 'destroy', 'update'
    ]]);

/*
|--------------------------------------------------------------------------
| Receipts Routes
|--------------------------------------------------------------------------
*/
    Route::get('/receipts', 'ReceiptController@index');
    Route::post('/receipts/create', 'ReceiptController@create');
    Route::post('/receipts', 'ReceiptController@create');
    Route::get('/receipts/{id}', 'ReceiptController@show');
    Route::delete('/receipts/{id}', 'ReceiptController@destroy');

/*
|--------------------------------------------------------------------------
| Referrals Routes
|--------------------------------------------------------------------------
*/
    Route::post('/referrals/create', 'ReferralController@create');
    Route::post('/referrals', 'ReferralController@create');
    Route::resource('referrals', 'ReferralController', ['only' => [
        'index', 'show', 'destroy', 'update'
    ]]);

/*
|--------------------------------------------------------------------------
| Profiles Routes
|--------------------------------------------------------------------------
*/
    Route::post('/profiles/create', 'ProfileController@create');
    Route::post('/profiles', 'ProfileController@create');
    Route::post('/profiles/export-pdf', 'ProfileController@exportPdf');
    Route::resource('profiles', 'ProfileController', ['only' => [
        'index', 'show', 'destroy', 'update'
    ]]);



/*
|--------------------------------------------------------------------------
| ServiceCenter Routes
|--------------------------------------------------------------------------
*/
    Route::get('/servicecenters/near',          'ServiceCenterController@findNear');
    Route::post('/servicecenters',              'ServiceCenterController@create');
    Route::post('/servicecenters/export-pdf',   'ServiceCenterController@exportPdf');
    Route::resource('servicecenters',           'ServiceCenterController', ['only' => [
        'index', 'show', 'update', 'destroy'
    ]]);

/*
|--------------------------------------------------------------------------
| Distributors Routes
|--------------------------------------------------------------------------
*/
    Route::get('/distributors/near',            'DistributorController@findNear');
    Route::post('/distributors',                'DistributorController@create');
    Route::post('/distributors/export-pdf',     'DistributorController@exportPdf');
    Route::resource('distributors',             'DistributorController', ['only' => [
        'index', 'show', 'update', 'destroy'
    ]]);



/*
|--------------------------------------------------------------------------
| Stats Routes
|--------------------------------------------------------------------------
*/
    Route::get('/stats', 'StatController@getTotals');
    Route::get('/stats/totals', 'StatController@getTotals');
    Route::get('/stats/thisWeek', 'StatController@getThisWeek');
    Route::get('/stats/totals/byDay', 'StatController@getTotalsByDay');
    Route::get('/stats/thisWeek/byDay', 'StatController@getThisWeekTotalsByDay');
    Route::get('/stats/thisWeek', 'StatController@getThisWeek');
    Route::get('/stats/lastWeek', 'StatController@getLastWeek');


 /*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
*/

    Route::get('/users/search/{type}/{id}',     'UserController@getUsersBy');


});
