@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Leads</div>

                <table class="panel-body table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Location</th>
                            <th>Registered on</th>
                            <th>Last login</th>
                            <th>Email</th>
                            <th>Opt-Ins</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($leads AS $lead)
                        <tr data-object="{{ $lead }}">
                            <td>{{ $lead->name }}</td>
                            <td>{{ $lead->age }}</td>
                            <td>{{ $lead->gender }}</td>
                            <td>{{ $lead->city }} {{ $lead->country }}</td>
                            <td>{{ $lead->created_at }}</td>
                            <td>{{ $lead->updated_at }}</td>
                            <td>{{ $lead->email }}</td>
                            <td>{{ $lead->optin_product_research }} {{ $lead->optin_promotions }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
